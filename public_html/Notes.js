var Notes = function () {
    
    this.domObj;
    this.drawObj = document.body;
    this.notesObject = {};

    
    this.draw = function (text) {
        var self = this;
        var notesCount = Object.keys(this.notesObject).length;
        this.domObj = document.createElement('div');
        this.domObj.style.position = 'absolute';
        this.domObj.style.right = '10px';
        this.domObj.style.top = (50*notesCount)+'px';
        this.domObj.style.height = '48px';
        this.domObj.style.background = '#555';
        this.domObj.style.color = '#fff';
        this.domObj.style.width = '250px';
        this.domObj.style.float = 'right';
        this.domObj.innerHTML = text ? text : 'Привіт!';
        this.drawObj.appendChild(this.domObj);

        this.close = document.createElement('div');
        this.close.style.heigth = '200px';
        this.close.style.width = '30px';
        this.close.style.textAlign = 'center';
        this.close.style.background = '#458';
        this.close.style.color = '#fff';
        this.close.innerHTML = '[x]';
        this.close.style.float = 'right';
        this.domObj.appendChild(this.close);

        var obj = this.domObj;
        
        this.domObj.timer = setTimeout(function () {
            self.drawObj.removeChild(obj);
            self.redraw(obj);
            delete self.notesObject[obj.timer];
        }, 5000);

        this.domObj.addEventListener('click', function () {
            clearTimeout(obj.timer);
        });
        
        this.close.addEventListener('click', function (){
            self.drawObj.removeChild(obj);
            self.redraw(obj);
            delete self.notesObject[obj.timer];
        });
        
        this.notesObject[this.domObj.timer] = this.domObj;
        
        this.redraw = function(obj){
            var flag = false;
            for(var n in self.notesObject){
                if(flag){
                    self.notesObject[n].style.top = (parseInt(self.notesObject[n].style.top)-50)+'px'; 
                }
                if(self.notesObject[n].timer == obj.timer){
                    flag = true;
                }
            }
        };
    };
};
