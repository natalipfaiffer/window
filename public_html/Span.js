var Span = function (target) {
    Basestyle.apply(this, arguments);
    Span.prototype = Object.create(Basestyle.prototype);
    Span.prototype.constructor = Span;

    var self = this;
    this.domElement;

    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    };

    var draw = function () {
        var span = document.createElement('span');
        self.span = span;
        self.domElement = span;
        target.domElement.appendChild(span);
    };

    draw();

};
Span.prototype.style = {};
