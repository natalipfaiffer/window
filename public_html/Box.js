var Box = function (target) {
    Basestyle.apply(this, arguments);
    Box.prototype = Object.create(Basestyle.prototype);
    Box.prototype.constructor = Box;

    var self = this;

    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    };

    var draw = function () {
        var box = document.createElement('div');
        self.box = box;
        self.domElement = box;
        target.domElement.appendChild(box);
    };

    draw();

};

Box.prototype.style = {};

