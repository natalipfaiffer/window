var Checkbox = function (target) {
    Basestyle.apply(this, arguments);
    Checkbox.prototype = Object.create(Basestyle.prototype);
    Checkbox.prototype.constructor = Checkbox;

    var self = this;
    self.id = getRandomId("Checkbox");
    this.group = "";

    var afterDraw = function (me) {
        if (me.group) {
            if (self.checkboxGroup[me.group]) {
                self.checkboxGroup[me.group].push(me);
            } else {
                self.checkboxGroup[me.group] = [me];
            }
        }
    };

    var draw = function () {
        var checkbox = new Input(target);
        self.checkbox = checkbox;
        checkbox.type('checkbox');
        self.domElement = checkbox.domElement;
        self.domElement.id = self.id;
        checkbox.onchange = function (e) {
            if (self.domElement.checked) {
                for (var i in self.checkboxGroup[self.group]) {
                    var checkbox = self.checkboxGroup[self.group][i];
                    if (checkbox.id !== self.id) {
                        checkbox.domElement.checked = false;
                    }
                }
            }
            self.events.onchange && self.events.onchange(e);
        };

        afterDraw(self);

    };

    draw();

};
Checkbox.prototype.style = {};
Checkbox.prototype.checkboxGroup = {};