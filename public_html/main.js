var mousemoveCallback = {};
document.onmousemove = function (e) {
    for (var i in mousemoveCallback) {
        mousemoveCallback[i](e);
    }
}
var onmouseupCallback = {};
document.onmouseup = function (e) {
    for (var i in onmouseupCallback) {
        onmouseupCallback[i](e, i);
    }
}
var readyDocument = function () {
    var mssdb = new ModelessDialogBox(document.body);
};

var currentId = 1;
function getRandomId(type) {
    return type + "" + currentId++;
}