var Fieldset = function (target, mode) {
    Basestyle.apply(this, arguments);
    Fieldset.prototype = Object.create(Basestyle.prototype);
    Fieldset.prototype.constructor = Fieldset;
    this.domElement;
    var self = this;

    this.id = getRandomId('Fieldset');
    this.statMode = mode;
    this.events = {};
    var fieldsetFlag = false;

    this.visible = function () {
        if (fieldsetFlag) {
            self.domElement.style.visibility = 'hidden';
            fieldsetFlag = false;
        } else {
            self.domElement.style.visibility = 'visible';
            fieldsetFlag = true;
        }
    };

    this.addElement = function (element) {
        self.domElement.appendChild(element.domContainer);
    };

    this.draw = function () {
        switch (this.mode()) {
            case 1:
                this.drawFsMode();
                break;
            case 2:
                this.drawBxMode();
                break;
            case 3:
                this.drawWsMode();
                break;
        }
    };

    this.mode = function (value) {
        if (value !== undefined) {
            this.statMode = value;
            this.draw();
        } else {
            return parseInt(this.statMode);
        }
    };

    this.drawBxMode = function () {

        var htmlBox = new Box(target);
        self.htmlBox = htmlBox;
        htmlBox.mode = 2;
        htmlBox.domElement.id = self.id;
        self.domElement = htmlBox.domElement;

        var elementArray = [];
        var newElementArray = [];

        var head = new Box(htmlBox);
        self.head = head;

        var closeBtn = new Button(head);
        self.closeBtn = closeBtn;

        var innerBox = new Box(htmlBox);
        self.innerBox = innerBox;

        var boxInner = new Box(innerBox);
        self.boxInner = boxInner;

        var inputLabel = new Form(boxInner, undefined, 1);
        self.inputLabel = inputLabel;
        elementArray.push(inputLabel);

        var label = new Form(boxInner, undefined, 5);
        self.label = label;
        elementArray.push(label);

        var input = new Form(boxInner, undefined, 4);
        self.input = input;
        elementArray.push(input);

        var array = [
            {value: 1, text: "Самозайнятий"},
            {value: 2, text: "до 3"},
            {value: 3, text: "до 5"},
            {value: 4, text: "до 10"},
            {value: 5, text: "7 вам"},
            {value: 6, text: "більше 50"},
        ];
        self.array = array;

        var select = new Form(boxInner, array, 2);
        self.select = select;
        elementArray.push(select);

        var checkbox = new Form(boxInner, undefined, 3);
        self.checkbox = checkbox;
        elementArray.push(checkbox);

        var textarea = new Form(boxInner, undefined, 6);
        self.textarea = textarea;
        elementArray.push(textarea);

        closeBtn.domElement.addEventListener("click", function () {
            self.hidden(true);
            self.neigbord.hideBox.hidden(false);
            storage.htmlBox.hidden = self.hidden();
            storage.hideBox.hidden = self.neigbord.hideBox.hidden();
            localStorage.setItem('storage', JSON.stringify(storage))

        });

        hide = function () {
            var screen = document.documentElement.clientWidth / 2;

            if (self.neigbord.domElement.getBoundingClientRect().right > screen) {
                function func() {
                    self.neigbord.hideBox.background("#fff");
                    self.neigbord.hideBox.border("1px solid #ececec");
                    storage.hideBox.background = self.neigbord.hideBox.background();
                    storage.hideBox.border = self.neigbord.hideBox.border();
                }
                setTimeout(func, 1000);
                self.neigbord.hideBox.hidden(false);
                self.hidden(true);
            } else {
                self.hidden(false);
            }

            storage.htmlBox.hidden = self.hidden();
            storage.hideBox.hidden = self.neigbord.hideBox.hidden();
            localStorage.setItem('storage', JSON.stringify(storage))

        };

        var createNewElement = function (dragElement) {

            var shiftX;
            var shiftY;
            var fixLeft;
            var fixTop;

            dragElement.domElement.addEventListener("mousedown", function (e) {
                dragElement.moveFlag = true;
                fixLeft = dragElement.domElement.offsetLeft;
                fixTop = dragElement.domElement.offsetTop;
                shiftX = e.pageX - fixLeft;
                shiftY = e.pageY - fixTop;
                moveAt(e);
            });

            function moveAt(e) {
                if ((e.pageX - shiftX) && document.documentElement.clientWidth > (e.pageX - shiftX) + parseInt(dragElement.width())) {
                    dragElement.left((e.pageX - shiftX) + "px");
                }
                if ((e.pageY - shiftY) && document.documentElement.clientHeight > (e.pageY - shiftY) + parseInt(dragElement.height())) {
                    dragElement.top((e.pageY - shiftY) + "px");
                }
            }

            this.id = getRandomId(dragElement.constructor.name);
            dragElement.id = this.id;

            var onmousemove = function (em) {
                if (dragElement.moveFlag) {
                    moveAt(em);
                }
            };

            var onmouseup = function (id) {
                var screen = document.documentElement.clientWidth / 1.5;
                if (id !== dragElement.id) {
                }
                if
                        (self.neigbord.innerBox.domElement.getBoundingClientRect().top < dragElement.domElement.getBoundingClientRect().top
                                &&
                                self.neigbord.innerBox.domElement.getBoundingClientRect().bottom > dragElement.domElement.getBoundingClientRect().bottom
                                &&
                                self.neigbord.innerBox.domElement.getBoundingClientRect().left < dragElement.domElement.getBoundingClientRect().left
                                &&
                                self.neigbord.innerBox.domElement.getBoundingClientRect().right <= screen
                                &&
                                self.neigbord.innerBox.domElement.getBoundingClientRect().right > dragElement.domElement.getBoundingClientRect().right
                                )
                {
                    dragElement.moveFlag = false;
                    Fieldset.addChild(self.neigbord, dragElement, newElementArray);
                    dragElement.top(fixTop + 'px');
                    dragElement.left(fixLeft + 'px');
                } else {
                    dragElement.moveFlag = false;
                    dragElement.webkitUserSelect('none');
                    dragElement.left(fixLeft + 'px');
                    dragElement.top(fixTop + 'px');
                }
            };

            onmouseupCallback[this.id] = onmouseup;
            mousemoveCallback[this.id] = onmousemove;

        };

        for (var i in elementArray) {
            createNewElement(elementArray[i]);
        }

    };
    this.drawWsMode = function () {
        var box = new Box(target);
        self.box = box;
        box.mode = 3;
        box.domElement.id = self.id;
        self.domElement = box.domElement;

        var head = new Box(box);
        self.head = head;

        var innerText = new Span(head);
        self.innerText = innerText;

        var iBtn = new Button(head);
        self.iBtn = iBtn;

        var saveBtn = new Button(head);
        self.saveBtn = saveBtn;

        var clearBtn = new Button(head);
        self.clearBtn = clearBtn;

        var editBtn = new Button(head);
        self.editBtn = editBtn;

        var closeBtn = new Button(head);
        self.closeBtn = closeBtn;

        var boxInner = new Box(box);
        self.boxInner = boxInner;

        var leftBorder = new Box(boxInner);
        self.leftBorder = leftBorder;

        var innerBox = new Box(boxInner);
        self.innerBox = innerBox;

        var rightBorder = new Box(boxInner);
        self.rightBorder = rightBorder;

        var bottomBorder = new Box(boxInner);
        self.bottomBorder = bottomBorder;

        var leftBottomBox = new Box(bottomBorder);
        self.leftBottomBox = leftBottomBox;

        var rightBottomBox = new Box(bottomBorder);
        self.rightBottomBox = rightBottomBox;

        closeBtn.domElement.addEventListener("click", function () {
            self.hidden(true);
        });

        head.moveFlag = false;
        var shiftX;
        var shiftY;

        head.domElement.onmousedown = function (e) {
            head.moveFlag = true;
            var coords = head.domElement.getBoundingClientRect(head);
            shiftX = e.pageX - coords.left;
            shiftY = e.pageY - coords.top;
            moveAt(e);
        };

        function moveAt(e) {
            if ((e.pageX - shiftX) > 0 && document.documentElement.clientWidth > (e.pageX - shiftX) + parseInt(box.width())) {
                box.left((e.pageX - shiftX) + "px");
                storage.box.left = box.left();
            }
            if ((e.pageY - shiftY) > 0 && document.documentElement.clientHeight > (e.pageY - shiftY) + parseInt(box.height())) {
                box.top((e.pageY - shiftY) + "px");
                storage.box.top = box.top();
            }
        }

        var minWidth = 500;
        var minHeight = 500;
        var minWidthBottomBorder = 537;
        var minHeightBox = 450;
        var dx;
        var dy;

        box.moveFlag = false;
        rightBorder.moveFlag = false;
        leftBorder.moveFlag = false;
        bottomBorder.moveFlag = false;
        leftBottomBox.moveFlag = false;
        rightBottomBox.moveFlag = false;

        rightBorder.domElement.onmousedown = function (e) {
            dx = e.pageX;
            rightBorder.moveFlag = true;
        };

        leftBorder.domElement.onmousedown = function (e) {
            dx = e.pageX;
            leftBorder.moveFlag = true;
        };

        bottomBorder.domElement.onmousedown = function (e) {
            dy = e.pageY;
            bottomBorder.moveFlag = true;
        };

        leftBottomBox.domElement.onmousedown = function (e) {
            dy = e.pageY;
            dx = e.pageX;
            leftBottomBox.moveFlag = true;
            leftBorder.moveFlag = false;
            bottomBorder.moveFlag = false;
        };

        rightBottomBox.domElement.onmousedown = function (e) {
            dy = e.pageY;
            dx = e.pageX;
            rightBottomBox.moveFlag = true;
            rightBorder.moveFlag = false;
            bottomBorder.moveFlag = false;
        };

        function rightResize(em) {
            if (rightBorder.moveFlag && self.neigbord.htmlBox.domElement.hidden == true && self.hideBox.domElement.hidden == false && (parseInt(box.width()) >= minWidth && (box.domElement.getBoundingClientRect().right <= self.hideBox.domElement.getBoundingClientRect().left))) {
                self.neigbord.htmlBox.domElement.hidden == true;

                var deltaX = (em.pageX - dx);

                box.width(parseInt(box.width()) + deltaX + "px");
                innerBox.width(innerBox.domElement.clientWidth + deltaX + "px");
                bottomBorder.width(bottomBorder.domElement.clientWidth + deltaX + "px");
                head.width(head.domElement.clientWidth + deltaX + "px");

                dx = em.pageX;

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();

            } else if (rightBorder.moveFlag && self.neigbord.htmlBox.domElement.hidden == false && ((parseInt(box.width()) >= minWidth) && (box.domElement.getBoundingClientRect().right <= self.neigbord.innerBox.domElement.getBoundingClientRect().left))) {
                var deltaX = (em.pageX - dx);

                box.width(parseInt(box.width()) + deltaX + "px");
                innerBox.width(innerBox.domElement.clientWidth + deltaX + "px");
                bottomBorder.width(bottomBorder.domElement.clientWidth + deltaX + "px");
                head.width(head.domElement.clientWidth + deltaX + "px");

                dx = em.pageX;

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();

            } else if (rightBorder.moveFlag) {
                box.width(minWidth + "px");
                head.width(minWidth + "px");
                innerBox.width(minWidth + "px");
                bottomBorder.width(minWidthBottomBorder + "px");

                rightBorder.moveFlag = false;

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.domElement.clientWidth;
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();

            }
        }

        function leftResize(em) {
            if ((leftBorder.moveFlag && parseInt(box.width())) >= minWidth) {
                var deltaX = (em.pageX - dx);

                box.width(parseInt(box.width()) - deltaX + "px");
                innerBox.width(innerBox.domElement.clientWidth - deltaX + "px");
                bottomBorder.width(bottomBorder.domElement.clientWidth - deltaX + "px");
                head.width(head.domElement.clientWidth - deltaX + "px");
                box.left(box.domElement.getBoundingClientRect().left + deltaX + "px");

                dx = em.pageX;

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();
                storage.box.left = box.left();

            } else if (leftBorder.moveFlag) {
                box.width(minWidth + "px");
                head.width(minWidth + "px");
                innerBox.width(minWidth + "px");
                bottomBorder.width(minWidthBottomBorder + "px");
                box.left(minWidth);

                leftBorder.moveFlag = false;

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();
                storage.box.left = box.left();
            }
        }

        function bottomResize(em) {
            if ((bottomBorder.moveFlag && parseInt(box.height())) >= minHeight && parseInt(box.height()) >= minHeight) {
                var deltaY = (em.pageY - dy);

                box.height(parseInt(box.height()) + deltaY + "px");
                leftBorder.height(parseInt(leftBorder.height()) + deltaY + "px");
                rightBorder.height(parseInt(rightBorder.height()) + deltaY + "px");
                boxInner.height(parseInt(boxInner.height()) + deltaY + "px");
                innerBox.height(parseInt(innerBox.height()) + deltaY + "px");

                dy = em.pageY;

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.innerBox.height = innerBox.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();

            } else if (bottomBorder.moveFlag) {
                box.height(minHeight + "px");
                leftBorder.height(minHeightBox + "px");
                rightBorder.height(minHeightBox + "px");
                innerBox.height(minHeightBox + "px");
                boxInner.height(minHeightBox + "px");

                bottomBorder.moveFlag = false;

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();
                storage.box.boxInner.innerBox.height = innerBox.height();

            }
        }

        function leftBottomResize(em) {
            if ((leftBottomBox.moveFlag && (box.domElement.clientWidth) >= minWidth) && (leftBottomBox.moveFlag && (box.domElement.clientHeight) >= minHeight)) {
                leftBorder.moveFlag = false;

                var deltaX = (em.pageX - dx);

                box.width(box.domElement.clientWidth - deltaX + "px");
                innerBox.width(innerBox.domElement.clientWidth - deltaX + "px");
                bottomBorder.width(bottomBorder.domElement.clientWidth - deltaX + "px");
                head.width(head.domElement.clientWidth - deltaX + "px");
                box.left(box.domElement.getBoundingClientRect().left + deltaX + "px");

                dx = em.pageX;

                var deltaY = (em.pageY - dy);

                box.height(parseInt(box.height()) + deltaY + "px");
                leftBorder.height(parseInt(leftBorder.height()) + deltaY + "px");
                rightBorder.height(parseInt(rightBorder.height()) + deltaY + "px");
                boxInner.height(parseInt(boxInner.height()) + deltaY + "px");
                innerBox.height(parseInt(innerBox.height()) + deltaY + "px");

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.innerBox.height = innerBox.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();
                storage.box.left = box.left();
                dy = em.pageY;

            } else if (leftBottomBox.moveFlag) {

                leftBorder.moveFlag = false;

                box.width(minWidth + "px");
                head.width(minWidth + "px");
                innerBox.width(minWidth + "px");
                bottomBorder.width(minWidthBottomBorder + "px");
                box.left(minWidth);

                box.height(minHeight + "px");
                leftBorder.height(minHeightBox + "px");
                rightBorder.height(minHeightBox + "px");
                innerBox.height(minHeightBox + "px");
                boxInner.height(minHeightBox + "px");

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.innerBox.height = innerBox.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();
                storage.box.left = box.left();

                leftBottomBox.moveFlag = false;

            }
        }
        function rightBottomResize(em) {
            if ((rightBottomBox.moveFlag && (box.domElement.clientHeight) >= minHeight) && (rightBottomBox.moveFlag && (box.domElement.clientWidth) >= minWidth)) {
                rightBorder.moveFlag = false;

                var deltaY = (em.pageY - dy);

                box.height(parseInt(box.height()) + deltaY + "px");
                leftBorder.height(parseInt(leftBorder.height()) + deltaY + "px");
                rightBorder.height(parseInt(rightBorder.height()) + deltaY + "px");
                innerBox.height(parseInt(innerBox.height()) + deltaY + "px");

                dy = em.pageY;

                var deltaX = (em.pageX - dx);

                box.width(box.domElement.clientWidth + deltaX + "px");
                innerBox.width(innerBox.domElement.clientWidth + deltaX + "px");
                bottomBorder.width(bottomBorder.domElement.clientWidth + deltaX + "px");
                head.width(head.domElement.clientWidth + deltaX + "px");

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.innerBox.height = innerBox.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();

                dx = em.pageX;

            } else if (rightBottomBox.moveFlag) {
                rightBorder.moveFlag = false;

                box.width(minWidth + "px");
                head.width(minWidth + "px");
                bottomBorder.width(minWidthBottomBorder + "px");
                innerBox.width(minWidth + "px");

                box.height(minHeight + "px");
                leftBorder.height(minHeight - head.height() + "px");
                rightBorder.height(minHeight - head.height() + "px");
                innerBox.height(minHeight - head.height() + "px");

                storage.box.height = box.height();
                storage.box.boxInner.height = boxInner.height();
                storage.box.boxInner.innerBox.height = innerBox.height();
                storage.box.boxInner.leftBorder.height = bottomBorder.height();
                storage.box.boxInner.rightBorder.height = head.height();

                storage.box.width = box.width();
                storage.box.boxInner.innerBox.width = innerBox.width();
                storage.box.boxInner.bottomBorder.width = bottomBorder.width();
                storage.box.head.width = head.width();

                rightBottomBox.moveFlag = false;

            }
        }


        var onmousemove = function (em) {
            if (rightBorder.moveFlag) {
                rightResize(em);
            }
            if (leftBorder.moveFlag) {
                leftResize(em);
            }
            if (bottomBorder.moveFlag) {
                bottomResize(em);
            }
            if (leftBottomBox.moveFlag) {
                leftBottomResize(em);
            }
            if (rightBottomBox.moveFlag) {
                rightBottomResize(em);
            }
            if (head.moveFlag) {
                moveAt(em);
            }
        };

        var onmouseup = function () {
            rightBorder.moveFlag = false;
            leftBorder.moveFlag = false;
            bottomBorder.moveFlag = false;
            leftBottomBox.moveFlag = false;
            rightBottomBox.moveFlag = false;
            head.moveFlag = false;
            box.moveFlag = false;
            document.body.onmousemove = null;
            localStorage.setItem('storage', JSON.stringify(storage));

        };

        onmouseupCallback[this.id] = onmouseup;
        mousemoveCallback[this.id] = onmousemove;

        self.editBtn.domElement.onmousedown = function (e) {
            e.stopPropagation();
            self.changeWs.left(box.left());
            self.changeWs.top(box.top());
            self.changeWs.width(box.width());
            self.changeWs.height(box.height());
            self.changeWs.hidden(false);
        };

        self.iBtn.domElement.onmousedown = function (e) {
            e.stopPropagation();
            self.helpWs.left(box.left());
            self.helpWs.top(box.top());
            self.helpWs.width(box.width());
            self.helpWs.height(box.height());
            self.helpWs.hidden(false);
        };

        self.saveBtn.domElement.onmousedown = function (e) {
            e.stopPropagation();
            localStorage.setItem('storage', JSON.stringify(storage));
            location.reload();
        };

        self.clearBtn.domElement.onmousedown = function (e) {
            e.stopPropagation();
            window.localStorage.clear()
            location.reload();

        };
    };

    this.draw();

};

Fieldset.prototype.style = {};

Fieldset.addChild = function (self, dragElement, newElementArray, id) {

    var dg, droped;
    if (dragElement instanceof Basestyle) {
        dragElement.moveFlag = false;
        droped = true;

        var border = self.box.borderWidth();
        var left = dragElement.domElement.getBoundingClientRect().left;
        var top = dragElement.domElement.getBoundingClientRect().top;
        var leftParent = self.box.domElement.getBoundingClientRect().left;
        var topParent = self.box.domElement.getBoundingClientRect().top;
        var leftFix = ((left - leftParent - border) + 'px');
        var topFix = ((top - topParent - border) + 'px');

        dg = {
            'objType': dragElement['objType'],
            'left': leftFix,
            'top': topFix,
            'width': dragElement.width(),
            'height': dragElement.height(),
            'display': dragElement.display(),
            'position': dragElement.position(),
            'background': dragElement.background(),
            'border': dragElement.border(),
            'color': dragElement.color(),
            'innerText': dragElement.innerText(),
            "boxBorder": dragElement.box.border(),
            "boxBoxSizing": dragElement.box.boxSizing(),
            "boxBackground": dragElement.box.background(),
            "boxWidth": dragElement.box.width(),
            "boxHeight": dragElement.box.height(),
            "boxFloat": dragElement.box.float(),
            "boxDisplay": dragElement.box.display(),
            "resizeBoxCursor": dragElement.resizeBox.cursor(),
            "resizeBoxBackground": dragElement.resizeBox.background(),
            "resizeBoxWidth": dragElement.resizeBox.width(),
            "resizeBoxHeight": dragElement.resizeBox.height(),
            "resizeBoxFloat": dragElement.resizeBox.float(),
            'labelColor': dragElement.label && dragElement.label.color(),
            'labelInnerText': dragElement.label && dragElement.label.innerText(),
            'labelWidth': dragElement.label && dragElement.label.width(),
            'labelHeight': dragElement.label && dragElement.label.height(),
            'labelDisplay': dragElement.label && dragElement.label.display(),
            'labelFloat': dragElement.label && dragElement.label.float(),
            'labelLineHeight': dragElement.label && dragElement.label.lineHeight(),
            'labelTextAlign': dragElement.label && dragElement.label.textAlign(),
            "inputBackground": dragElement.input && dragElement.input.background(),
            "inputColor": dragElement.input && dragElement.input.color(),
            "inputWidth": dragElement.input && dragElement.input.width(),
            "inputHeight": dragElement.input && dragElement.input.height(),
            "inputDisplay": dragElement.input && dragElement.input.display(),
            "inputBorder": dragElement.input && dragElement.input.border(),
            "inputFloat": dragElement.input && dragElement.input.float(),
            "inputBoxSizing": dragElement.input && dragElement.input.boxSizing(),
            "inputPlaceholder": dragElement.input && dragElement.input.placeholder(),
            "inputPlaceholderColor": dragElement.input && dragElement.input.innerHTML,
            "inputLineHeight": dragElement.input && dragElement.input.lineHeight(),
            "selectBackground": dragElement.select && dragElement.select.background(),
            "selectBorderColor": dragElement.select && dragElement.select.border(),
            "selectColor": dragElement.select && dragElement.select.color(),
            "selectWidth": dragElement.select && dragElement.select.width(),
            "selectHeight": dragElement.select && dragElement.select.height(),
            "selectDisplay": dragElement.select && dragElement.select.display(),
            "selectFloat": dragElement.select && dragElement.select.float(),
            "selectLineHeight": dragElement.select && dragElement.select.lineHeight(),
            "checkboxBackground": dragElement.checkbox && dragElement.checkbox.background(),
            "checkboxWidth": dragElement.checkbox && dragElement.checkbox.width(),
            "checkboxHeight": dragElement.checkbox && dragElement.checkbox.height(),
            "checkboxDisplay": dragElement.checkbox && dragElement.checkbox.display(),
            "checkboxFloat": dragElement.checkbox && dragElement.checkbox.float(),
            "checkboxMarginTop": dragElement.checkbox && dragElement.checkbox.marginTop(),
            "textareaBackground": dragElement.textarea && dragElement.textarea.background(),
            "textareaWidth": dragElement.textarea && dragElement.textarea.width(),
            "textareaHeight": dragElement.textarea && dragElement.textarea.height(),
            "textareaDisplay": dragElement.textarea && dragElement.textarea.display(),
            "textareaFloat": dragElement.textarea && dragElement.textarea.float(),
            "textareaPlaceholder": dragElement.textarea && dragElement.textarea.placeholder(),
            "textareaResize": dragElement.textarea && dragElement.textarea.resize(),
            "textareaBorder": dragElement.textarea && dragElement.textarea.border(),
            "placeholderColorTextarea": dragElement.textarea && dragElement.textarea.innerHTML,
            "array": [

            ], };
    } else {
        dg = dragElement;
    }

    switch (dragElement['objType']) {

        case 'InputLabel':
            var newElement = new Form(self, undefined, 1, true);
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: undefined,
                    mode: 1,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

        case 'Select':
            var newElement = new Form(self, dg.array, 2, true);
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: dg.array,
                    mode: 2,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

        case 'Checkbox':
            var newElement = new Form(self, undefined, 3, true);
            newElement.droped = true;
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: undefined,
                    mode: 3,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

        case 'Input':
            var newElement = new Form(self, undefined, 4, true);
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: undefined,
                    mode: 4,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

        case 'Label':
            var newElement = new Form(self, undefined, 5, true);
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: undefined,
                    mode: 5,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

        case 'Textarea':
            var newElement = new Form(self, undefined, 6, true);
            newElementArray.push(newElement);
            if (droped) {
                storage.childs[newElement.id] = {
                    array: undefined,
                    mode: 6,
                    dragElement: dg,
                };
            } else {
                newElement.id = id;
            }
            break;

    }

    if (droped) {
        newElement.droped = true;
    }

    newElement.parent = self.box.domElement;
    newElement.parentInstance = self.box;
    self.newElement = newElement;
    newElement.moveFlag = false;
    newElement.webkitUserSelect('none');
    newElement.position(dg.position);
    newElement.border(dg.border);
    newElement.display(dg.display);
    newElement.height(dg.height);
    newElement.background(dg.background);
    newElement.width(dg.width);
    newElement.left(dg.left);
    newElement.top(dg.top);

    newElement.box.boxSizing(dg.boxBoxSizing);
    newElement.box.background(dg.boxBackground);
    newElement.box.width(dg.boxWidth);
    newElement.box.border(dg.boxBorder);
    newElement.box.height(dg.boxHeight);
    newElement.box.float(dg.boxFloat);
    newElement.box.display(dg.boxDisplay);

    newElement.resizeBox.cursor(dg.resizeBoxCursor);
    newElement.resizeBox.background(dg.resizeBoxBackground);
    newElement.resizeBox.width(dg.resizeBoxWidth);
    newElement.resizeBox.height(dg.resizeBoxHeight);
    newElement.resizeBox.float(dg.resizeBoxFloat);

    (newElement.label) && newElement.label.innerText(dg.labelInnerText);
    (newElement.label) && newElement.label.width(dg.labelWidth);
    (newElement.label) && newElement.label.height(dg.labelHeight);
    (newElement.label) && newElement.label.display(dg.labelDisplay);
    (newElement.label) && newElement.label.float(dg.labelFloat);
    (newElement.label) && newElement.label.lineHeight(dg.labelLineHeight);
    (newElement.label) && newElement.label.textAlign(dg.labelTextAlign);
    (newElement.label) && newElement.label.color(dg.labelColor);

    (newElement.input) && newElement.input.background(dg.inputBackground);
    (newElement.input) && newElement.input.width(dg.inputWidth);
    (newElement.input) && newElement.input.height(dg.inputHeight);
    (newElement.input) && newElement.input.display(dg.inputDisplay);
    (newElement.input) && newElement.input.border(dg.inputBorder);
    (newElement.input) && newElement.input.float(dg.inputFloat);
    (newElement.input) && newElement.input.boxSizing(dg.inputBoxSizing);
    (newElement.input) && newElement.input.placeholder(dg.inputPlaceholder);
    (newElement.input) && newElement.input.lineHeight(dg.inputLineHeight);

    (newElement.select) && newElement.select.background(dg.selectBackground);
    (newElement.select) && newElement.select.color(dg.selectColor);
    (newElement.select) && newElement.select.width(dg.selectWidth);
    (newElement.select) && newElement.select.height(dg.selectHeight);
    (newElement.select) && newElement.select.display(dg.selectDisplay);
    (newElement.select) && newElement.select.float(dg.selectFloat);
    (newElement.select) && newElement.select.lineHeight(dg.selectLineHeight);
    (newElement.select) && newElement.select.pointerEvents('auto');
    (newElement.select) && newElement.select.borderColor(dg.selectBorderColor);

    (newElement.checkbox) && newElement.checkbox.background(dg.checkboxBackground);
    (newElement.checkbox) && newElement.checkbox.width(dg.checkboxWidth);
    (newElement.checkbox) && newElement.checkbox.height(dg.checkboxHeight);
    (newElement.checkbox) && newElement.checkbox.display(dg.checkboxDisplay);
    (newElement.checkbox) && newElement.checkbox.float(dg.checkboxFloat);
    (newElement.checkbox) && newElement.checkbox.marginTop(dg.checkboxMarginTop + 'px');

    (newElement.textarea) && newElement.textarea.background(dg.textareaBackground);
    (newElement.textarea) && newElement.textarea.width(dg.textareaWidth);
    (newElement.textarea) && newElement.textarea.height(dg.textareaHeight);
    (newElement.textarea) && newElement.textarea.display(dg.textareaDisplay);
    (newElement.textarea) && newElement.textarea.float(dg.textareaFloat);
    (newElement.textarea) && newElement.textarea.placeholder(dg.textareaPlaceholder);
    (newElement.textarea) && newElement.textarea.resize(dg.textareaResize);
    (newElement.textarea) && newElement.textarea.border(dg.textareaBorder);

    if (newElement.textarea && dg.placeholderColorTextarea && !droped) {
        if (typeof dg.placeholderColorTextarea == 'function') {
            newElement.textarea.styleElem.innerHTML = dg.placeholderColorTextarea;
        } else {
            var newIdTextarea = newElement.textarea.domElement.id;
            var resumeIdTextarea = "#" + newIdTextarea + "::" + dg.placeholderColorTextarea.split("::")[1];
            newElement.textarea.styleElem.innerHTML = resumeIdTextarea;
        }
    } else if (newElement.input && dg.inputPlaceholderColor && !droped) {
        if (typeof dg.inputPlaceholderColor == 'function') {
            newElement.input.styleElem.innerHTML = dg.inputPlaceholderColor;
        } else {
            var newIdInput = newElement.input.domElement.id;
            var resumeIdInput = "#" + newIdInput + "::" + dg.inputPlaceholderColor.split("::")[1];
            newElement.input.styleElem.innerHTML = resumeIdInput;
        }
    }

    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea || newElement.label) || newElement.resizeBox && newElement.resizeBox.background(dg.resizeBoxBackground);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea || newElement.label) || newElement.resizeBox && newElement.resizeBox.width(dg.resizeBoxWidth);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea || newElement.label) || newElement.resizeBox && newElement.resizeBox.height(dg.resizeBoxHeight);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea || newElement.label) || newElement.resizeBox && newElement.resizeBox.float(dg.resizeBoxFloat);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea || newElement.label) || newElement.resizeBox && newElement.resizeBox.cursor(dg.resizeBoxCursor);

    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.background(dg.boxBackground);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.border(dg.boxBorder);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.width(dg.boxWidth);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.height(dg.boxHeight);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.float(dg.boxFloat);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.boxSizing(dg.boxBoxSizing);
    (newElement.input || newElement.select || newElement.checkbox || newElement.textarea) && newElement.box.display(dg.boxDisplay);
};


