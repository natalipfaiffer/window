var Input = function (target) {
    Basestyle.apply(this, arguments);
    Input.prototype = Object.create(Basestyle.prototype);
    Input.prototype.constructor = Input;

    var self = this;
    this.id = getRandomId('Input');
    
    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    };

    var draw = function () {
        var input = document.createElement('input');
        var styleElem = document.head.appendChild(document.createElement("style"));
        self.styleElem = styleElem;
        self.input = input;
        self.domElement = input;
        this.id = getRandomId(input.constructor.name);
        input.id = this.id;
        target.domElement.appendChild(input);
    };

    draw();

};

Input.prototype.style = {};

