var List = function (target, arrayList) {

    Basestyle.apply(this, arguments);
    List.prototype = Object.create(Basestyle.prototype);
    List.prototype.constructor = List;
    var self = this;

    var draw = function () {
        var list = document.createElement("ul");
        self.list = list;
        self.domElement = list;
        target.domElement.appendChild(list);

        var li = document.createElement("li");
        self.liDomElement = li;
        li.value = 0;
        list.appendChild(li);
        for (var i in arrayList) {
            var li = document.createElement("li");
            li.value = arrayList[i].value;
            li.innerText = arrayList[i].text;
            list.appendChild(li);
        }
 };
 
    draw();
    
};

List.prototype.style = {};
