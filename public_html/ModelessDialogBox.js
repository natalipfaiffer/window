var storage = {

    "box": {
        "borderColor": "rgba(166, 70, 98, 1)",
        "borderWidth": "5px",
        "borderStyle": "solid",
        "height": "500px",
        "width": "widthFlag",
        "background": "#fff",
        "position": "absolute",
        "left": "2.5%",
        "top": "2.5%",
        "head": {
            "height": "50px",
            "width": "widthFlag",
            "webkitUserSelect": "none",
            "background": "#a64662",
            "paddingTop": "5px",
            "boxSizing": "border-box",
            "fontSize": "25px",
            "textAlign": "center",
            "textShadow": "1px 2px 2px #000",
            "lineHeight": "26px",
            "innerText": {
                "innerText": "Drag and Drop!",
                "width": "100%",
                "color": "#dadadf",
                "background": "#a64662",
                "paddingTop": "5px",
                "boxSizing": "border-box",
                "fontSize": "25px",
                "textAlign": "center",
                "textShadow": "1px 2px 2px #000",
                "lineHeight": "26px",
            },

            "iBtn": {
                "type": 'button',
                "innerHTML": '?',
                "color": '#dadadf',
                "background": 'rgba(255, 119, 153, 0)',
                "right": '150px',
                "top": '2px',
                "fontSize": '24px',
                "fontStyle": 'bold',
                "position": 'absolute',
                "padding": '0,0,0,0',
                "textShadow": 'rgb(0, 0, 0) 1px 2px 2px',
                "border": '0px solid #000',
                "zIndex": '2',
            },

            "saveBtn": {
                "type": "button",
                "right": "-5px",
                "innerHTML": "&#xf0c7",
                "color": "#dadadf",
                "background": "rgba(255, 119, 153, 0)",
                "fontSize": "25px",
                "position": "absolute",
                "padding": "0,0,0,0",
                "textShadow": "rgb(0, 0, 0) 1px 2px 2px",
                "border": "0px solid #000",
                "zIndex": "2",
            },
            "closeBtn": {
                "type": "button",
                "right": "-5px",
                "innerHTML": "&#xf00d",
                "color": "#dadadf",
                "background": "rgba(255, 119, 153, 0)",
                "fontSize": "25px",
                "position": "absolute",
                "padding": "0,0,0,0",
                "textShadow": "rgb(0, 0, 0) 1px 2px 2px",
                "border": "0px solid #000",
                "zIndex": "2",
            },
            "editBtn": {
                "type": "button",
                "right": "-5px",
                "innerHTML": "&#xf044",
                "color": "#dadadf",
                "background": "rgba(255, 119, 153, 0)",
                "fontSize": "25px",
                "position": "absolute",
                "padding": "0,0,0,0",
                "textShadow": "rgb(0, 0, 0) 1px 2px 2px",
                "border": "0px solid #000",
                "zIndex": "2",
            },
            "clearBtn": {
                "type": "button",
                "right": "-5px",
                "innerHTML": "&#xf1f8",
                "color": "#dadadf",
                "background": "rgba(255, 119, 153, 0)",
                "fontSize": "25px",
                "position": "absolute",
                "padding": "0,0,0,0",
                "textShadow": "rgb(0, 0, 0) 1px 2px 2px",
                "border": "0px solid #000",
                "zIndex": "2",
            },
        },
        "boxInner": {
            "position": "absolute",
            "width": "100%",
            "height": "450px",
            "innerBox": {
                "height": "450px",
                "width": "100%",
                "position": "#439"
            },
            "leftBorder": {
                "width": "10px",
                "height": "460px",
                "left": "-7px",
                "cursor": "w-resize",
                "fontSize": "25px",
                "position": "absolute",
                "zIndex": "3",
            },
            "rightBorder": {
                "width": "10px",
                "height": "460px",
                "right": "-7px",
                "cursor": "w-resize",
                "fontSize": "25px",
                "position": "absolute",
                "zIndex": "4",
            },
            "bottomBorder": {
                "width": "widthFlag",
                "height": "10px",
                "bottom": "-5px",
                "left": "-12px",
                "cursor": "s-resize",
                "position": "absolute",
                "zIndex": "4",
                "leftBottomBox": {
                    "width": "30px",
                    "height": "30px",
                    "float": "left",
                    "margin": "0, 0, -3.5px, -3.5px",
                    "left": "-12px",
                    "cursor": "nesw-resize",
                    "zIndex": "12",
                },
                "rightBottomBox": {
                    "width": "30px",
                    "height": "30px",
                    "float": "right",
                    "left": "-12px",
                    "cursor": "nwse-resize",
                    "zIndex": "12",
                },
            },
        },
    },

    childs: {},

    "htmlBox": {
        "hidden": false,
        "borderColor": "rgba(166, 70, 98, 1)",
        "borderWidth": "5px",
        "borderStyle": "solid",
        "height": "500px",
        "width": "realWidth",
        "background": "#fff",
        "position": "absolute",
        "left": "2.5%",
        "top": "2.5%",
        "head": {
            "height": "50px",
            "width": "100%",
            "webkitUserSelect": "none",
            "background": "#a64662",
            "paddingTop": "5px",
            "boxSizing": "border-box",
            "fontSize": "25px",
            "textAlign": "center",
            "textShadow": "1px 2px 2px #000",
            "lineHeight": "26px",
            "closeBtn": {
                "type": "button",
                "right": "-5px",
                "innerHTML": "&#xD",
                "color": "#dadadf",
                "background": "rgba(255, 119, 153, 0)",
                "fontSize": "25px",
                "position": "absolute",
                "padding": "0,0,0,0",
                "textShadow": "rgb(0, 0, 0) 1px 2px 2px",
                "border": "0px solid #000",
                "zIndex": "2",
            },

        },
        "innerBox": {
            "zIndex": "2",
            "width": "100%",
            "display": "flex",
            "height": "450px",
            "boxInner": {
                "height": "325px",
                "width": "100%",
                "zIndex": "2",
                "margin": "auto",
                "inputLabel": {
                    "position": "absolute",
                    "width": "480px",
                    "height": "30px",
                    "cursor": "pointer",
                    "left": "70px",
                    "top": "110px",
                    "box": {
                        "border": "1px solid rgb(166, 70, 98)",
                        "boxSizing": "border-box",
                        "width": "98%",
                        "height": "30px",
                        "float": "left",
                        "background": "#fff",
                        "cursor": "pointer",
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "30px",
                        "float": "right",
                        "cursor": "e-resize",
                    },
                    "label": {
                        "innerText": "Введіть свої дані",
                        "boxSizing": "border-box",
                        "width": "49%",
                        "height": "28px",
                        "float": "left",
                        "textAlign": "center",
                        "lineHeight": "25px",
                        "fontSize": "16px",
                        "color": "#000",
                    },
                    "input": {
                        "innerText": "Введіть свої дані",
                        "background": "#fff",
                        "display": "block",
                        "width": "49%",
                        "height": "28px",
                        "float": "right",
                        "boxSizing": "border-box",
                    },
                },
                "select": {
                    "position": "absolute",
                    "cursor": "pointer",
                    "height": "30px",
                    "width": "480px",
                    "left": "70px",
                    "top": "270px",
                    "webkitUserSelect": "none",
                    "UserSelect": "none",
                    "mozUserSelect": "none",
                    "msUserSelect": "none",
                    "box": {
                        "border": "1px solid rgb(166, 70, 98)",
                        "boxSizing": "border-box",
                        "width": "98%",
                        "height": "30px",
                        "float": "left",
                        "background": "#fff",
                        "cursor": 'pointer',
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "30px",
                        "float": "right",
                        "cursor": 'e-resize',
                    },
                    "select": {
                        "display": "block",
                        "pointerEvents": "none",
                        "width": "100%",
                        "height": "28px",
                        "float": "right",
                    },
                },
                "input": {
                    "position": "absolute",
                    "cursor": "pointer",
                    "height": "30px",
                    "width": "480px",
                    "left": "70px",
                    "top": "165px",
                    "box": {
                        "border": "1px solid rgb(166, 70, 98)",
                        "boxSizing": "border-box",
                        "width": "98%",
                        "height": "30px",
                        "float": "left",
                        "background": "#fff",
                        "cursor": 'pointer',
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "25px",
                        "float": "right",
                        "cursor": 'e-resize',
                    },
                    "input": {
                        "placeholder": "введіть значення",
                        "color": "#129",
                        "boxSizing": "border-box",
                        "width": "100%",
                        "height": "28px",
                        "float": "right",
                        "background": "#fff'",
                    },
                },
                "label": {
                    "position": "absolute",
                    "cursor": "pointer",
                    "height": "30px",
                    "width": "480px",
                    "left": "70px",
                    "top": "220px",
                    "box": {
                        "border": "1px solid rgb(166, 70, 98)",
                        "boxSizing": "border-box",
                        "width": "98%",
                        "height": "30px",
                        "float": "left",
                        "background": "#fff",
                        "cursor": 'pointer',
                        "display": 'block',
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "25px",
                        "float": "right",
                        "cursor": 'e-resize',
                    },
                    "label": {
                        "innerText": "Введіть значення",
                        "width": "100%",
                        "height": "28px",
                        "lineHeight": "25px",
                        "textAlign": "center",
                        "display": "block",
                    },
                },
                "checkbox": {
                    "position": "absolute",
                    "cursor": "pointer",
                    "height": "30px",
                    "width": "480px",
                    "left": "70px",
                    "top": "330px",
                    "boxSizing": "border-box",
                    "box": {
                        "border": "1px solid rgb(166, 70, 98)",
                        "boxSizing": "border-box",
                        "width": "98%",
                        "height": "30px",
                        "float": "left",
                        "background": "#fff",
                        "cursor": 'pointer',
                        "display": 'flex',
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "25px",
                        "float": "right",
                        "cursor": 'e-resize',
                    },
                    "label": {
                        "innerText": "Введіть свої дані",
                        "width": "25px",
                        "fontSize": "16px",
                        "height": "100%",
                        "float": "left",
                        "display": "block",
                        "textAlign": "center",
                        "lineHeight": "25px",
                    },
                    "checkbox": {
                        "width": "25px",
                        "height": "25px",
                        "float": "left",
                        "marginTop": "2px",
                        "border": "none",
                    },
                },
                "textarea": {
                    "position": "absolute",
                    "cursor": "pointer",
                    "height": "50px",
                    "width": "480px",
                    "left": "70px",
                    "top": "390px",
                    "border": "1px solid rgb(166, 70, 98)",
                    "box": {
                        "float": "left",
                        "boxSizing": "border-box",
                        "display": 'block',
                        "width": "98%",
                        "height": "50px",
                        "background": "#fff",
                        "cursor": 'pointer',
                    },
                    "resizeBox": {
                        "width": "2%",
                        "height": "50px",
                        "float": "right",
                        "cursor": 'e-resize',
                    },
                    "textarea": {
                        "placeholder": "Введіть свої дані",
                        "width": "98.79%",
                        "height": "42px",
                        "display": "block",
                        "background": "#fff",
                        "resize": "none",
                        "border": "none",
                    },
                },
            },
        }
    },

    "hideBox": {
        "hidden": true,
        "position": "absolute",
        "width": "25px",
        "height": "150px",
        "background": "#597",
        "border": "1px solid #fff",
        "boxSizing": "border-box",
        "right": "0",
        "bottom": "50%"
    }

};

var ModelessDialogBox = function () {
    this.domElement;
    var self = this;
    this.elements = [];
    var screen = document.documentElement.clientWidth;

    var draw = function () {

        if ((localStorage.getItem('storage'))) {
            storage = (JSON.parse(localStorage.getItem('storage')));
        }

        var box = new Fieldset({domElement: document.body}, 3);
        var realWidth = ((screen * 45 / 100) + 'px');
        var bottomWidth = ((parseInt(realWidth) + 36) + 'px');

        self.domElement = box.domElement;
        box.parent = self.box;
        box.hidden(false);
        box.borderWidth('5px');
        box.borderColor(storage.box.borderColor);
        box.borderStyle('solid');
        box.height(storage.box.height);

        if (storage.box.width === "widthFlag" || storage.box.head.width === "widthFlag" || storage.box.boxInner.innerBox.width === "widthFlag") {
            box.width(realWidth);
            box.head.width(realWidth);
        } else {
            box.width(storage.box.width);
            box.head.width(storage.box.head.width);
        }

        box.background('#fff');
        box.position('absolute');
        box.left(storage.box.left);
        box.top(storage.box.top);

        box.head.height('50px');
        box.head.zIndex('12');
        box.head.width(storage.box.head.width);
        box.head.webkitUserSelect('none');
        box.head.background(storage.box.head.background);
        box.head.boxSizing('border-box');
        box.head.paddingTop('5px');
        box.head.textShadow('1px 2px 2px #000');
        box.head.lineHeight('26px');
        box.head.cursor('pointer');

        box.innerText.innerText(storage.box.head.innerText.innerText);
        box.innerText.width('100%');
        box.innerText.left("38%");
        box.innerText.position('relative');
        box.innerText.fontSize('23px');
        box.innerText.color(storage.box.head.innerText.color);

        box.iBtn.type('button');
        box.iBtn.innerHTML('?');
        box.iBtn.color(storage.box.head.iBtn.color);
        box.iBtn.background('rgba(255, 119, 153, 0)');
        box.iBtn.right('150px');
        box.iBtn.top('2px');
        box.iBtn.fontSize('24px');
        box.iBtn.fontStyle('bold');
        box.iBtn.position('absolute');
        box.iBtn.padding('0,0,0,0');
        box.iBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        box.iBtn.border('0px solid #000');
        box.iBtn.zIndex('2');

        box.saveBtn.type('button');
        box.saveBtn.innerHTML('&#xf0c7');
        box.saveBtn.color(storage.box.head.saveBtn.color);
        box.saveBtn.background('rgba(255, 119, 153, 0)');
        box.saveBtn.right('110px');
        box.saveBtn.fontSize('20px');
        box.saveBtn.position('absolute');
        box.saveBtn.padding('0,0,0,0');
        box.saveBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        box.saveBtn.border('0px solid #000');
        box.saveBtn.zIndex('2');

        box.clearBtn.type('button');
        box.clearBtn.innerHTML('&#xf1f8');
        box.clearBtn.color(storage.box.head.clearBtn.color);
        box.clearBtn.background('rgba(255, 119, 153, 0)');
        box.clearBtn.right('70px');
        box.clearBtn.fontSize('20px');
        box.clearBtn.position('absolute');
        box.clearBtn.padding('0,0,0,0');
        box.clearBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        box.clearBtn.border('0px solid #000');
        box.clearBtn.zIndex('2');

        box.editBtn.type('button');
        box.editBtn.innerHTML('&#xf044');
        box.editBtn.color(storage.box.head.editBtn.color);
        box.editBtn.background('rgba(255, 119, 153, 0)');
        box.editBtn.right('30px');
        box.editBtn.fontSize('22px');
        box.editBtn.position('absolute');
        box.editBtn.padding('0,0,0,0');
        box.editBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        box.editBtn.border('0px solid #000');
        box.editBtn.zIndex('2');

        box.closeBtn.type('button');
        box.closeBtn.right("-2px");
        box.closeBtn.innerHTML('&#xf00d');
        box.closeBtn.color(storage.box.head.closeBtn.color);
        box.closeBtn.background('rgba(255, 119, 153, 0)');
        box.closeBtn.fontSize('23px');
        box.closeBtn.position('absolute');
        box.closeBtn.padding('0,0,0,0');
        box.closeBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        box.closeBtn.border('0px solid #000');
        box.closeBtn.zIndex('2');

        box.boxInner.height(storage.box.boxInner.height);
        box.boxInner.width('100%');

        box.leftBorder.width('10px');
        box.leftBorder.height(storage.box.boxInner.height);
        box.leftBorder.position("absolute");
        box.leftBorder.left('-7px');
        box.leftBorder.cursor("w-resize");
        box.leftBorder.zIndex(5);

        box.innerBox.width('100%');
        box.innerBox.height(storage.box.boxInner.height);
        box.innerBox.position('absolute');

        box.rightBorder.width('10px');
        box.rightBorder.height(storage.box.boxInner.height);
        box.rightBorder.position('absolute');
        box.rightBorder.right('-7px');
        box.rightBorder.cursor("e-resize");
        box.rightBorder.zIndex(5);

        if (storage.box.boxInner.bottomBorder.width === "widthFlag") {
            box.bottomBorder.width(bottomWidth);
        } else {
            box.bottomBorder.width(storage.box.boxInner.bottomBorder.width);
        }

        box.bottomBorder.width(storage.box.boxInner.bottomBorder.width);
        box.bottomBorder.height('10px');
        box.bottomBorder.position('absolute');
        box.bottomBorder.bottom('-10px');
        box.bottomBorder.left('-18px');
        box.bottomBorder.cursor("s-resize");
        box.bottomBorder.zIndex(5);

        box.leftBottomBox.width('30px');
        box.leftBottomBox.height('30px');
        box.leftBottomBox.float('left');
        box.leftBottomBox.cursor("nesw-resize");
        box.leftBottomBox.margin('0, 0, -3.5px, -3.5px');
        box.leftBottomBox.zIndex(12);
        box.leftBottomBox.marginTop('-10px');

        box.rightBottomBox.width('30px');
        box.rightBottomBox.height('30px');
        box.rightBottomBox.float("right");
        box.rightBottomBox.cursor('nwse-resize');
        box.rightBottomBox.marginTop('-10px');
        box.rightBottomBox.zIndex(12);
        self.elements.push(box);

        var newElementArr = [];

        for (var i in storage.childs) {
            Fieldset.addChild(box, storage.childs[i].dragElement, newElementArr, i); //add elements at restoration localstorage
        }

        var changeWs = new Redactor({domElement: document.body}, 1);
        self.domElement = changeWs.domElement;
        box.changeWs = changeWs;

        changeWs.onClick = function () {
            box.borderColor(changeWs.borderBackground.input.value());
            box.head.background(changeWs.borderBackground.input.value());
            box.innerText.color(changeWs.textColor.input.value());
            box.innerText.innerText(changeWs.textInner.input.value());
            box.iBtn.color(changeWs.textColor.input.value());
            box.editBtn.color(changeWs.textColor.input.value());
            box.saveBtn.color(changeWs.textColor.input.value());
            box.clearBtn.color(changeWs.textColor.input.value());
            box.closeBtn.color(changeWs.textColor.input.value());
            storage.box.borderColor = changeWs.borderBackground.input.value();
            storage.box.head.background = changeWs.borderBackground.input.value();
            storage.box.head.innerText.color = changeWs.textColor.input.value();
            storage.box.head.innerText.innerText = changeWs.textInner.input.value();
            storage.box.head.iBtn.color = changeWs.textColor.input.value();
            storage.box.head.editBtn.color = changeWs.textColor.input.value();
            storage.box.head.saveBtn.color = changeWs.textColor.input.value();
            storage.box.head.clearBtn.color = changeWs.textColor.input.value();
            storage.box.head.closeBtn.color = changeWs.textColor.input.value();
            localStorage.setItem('storage', JSON.stringify(storage));

        };

        changeWs.hidden(true);
        changeWs.position('absolute');
        changeWs.width(box.width());
        changeWs.height(box.height());
        changeWs.border('5px solid rgba(166, 70, 98, 1)');
        changeWs.background('#fff');
        changeWs.zIndex(2);
        changeWs.left('2.5%');
        changeWs.top('2.5%');

        changeWs.head.background('rgba(166, 70, 98, 1)');
        changeWs.head.zIndex(4);
        changeWs.head.textAlign('center');
        changeWs.head.textShadow('1px 2px 2px #000');
        changeWs.head.lineHeight('26px');
        changeWs.head.height('50px');
        changeWs.head.width('100%');
        changeWs.head.cursor('pointer');
        changeWs.head.top('0px');
        changeWs.head.left('0px');

        changeWs.closeBtn.type('button');
        changeWs.closeBtn.right('0');
        changeWs.closeBtn.innerHTML('&#xD7');
        changeWs.closeBtn.border('none');
        changeWs.closeBtn.color('rgb(255, 119, 153)');
        changeWs.closeBtn.background('rgba(166, 70, 98, 1)');
        changeWs.closeBtn.fontSize('25px');
        changeWs.closeBtn.position('absolute');
        changeWs.closeBtn.padding('0,0,0,0');
        changeWs.closeBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        changeWs.closeBtn.zIndex(5);

        changeWs.innerBox.width('100%');
        changeWs.innerBox.height('450px');
        changeWs.innerBox.display('flex');

        changeWs.boxInner.border('3px solid rgba(166, 70, 98, 1)');
        changeWs.boxInner.padding('5px');
        changeWs.boxInner.width('70%');
        changeWs.boxInner.height('auto');
        changeWs.boxInner.zIndex(3);
        changeWs.boxInner.margin('auto');
        changeWs.boxInner.border('5px solid rgba(166, 70, 98, 1)');
        changeWs.boxInner.padding('25px');
        changeWs.boxInner.zIndex(3);
        changeWs.boxInner.margin('auto');

        changeWs.textColor.width('100%');
        changeWs.textColor.zIndex(7);
        changeWs.textColor.height('25px');
        changeWs.textColor.marginBottom('25px');

        changeWs.textColor.label.width('50%');
        changeWs.textColor.label.height('25px');
        changeWs.textColor.label.float('left');
        changeWs.textColor.label.innerText('Change the text color on head');

        changeWs.textColor.input.type('color');
        changeWs.textColor.input.width('50%');
        changeWs.textColor.input.height('25px');
        changeWs.textColor.input.float('right');
        changeWs.textColor.input.boxSizing('border-box');
        changeWs.textColor.input.borderWidth(1, 1, 1, 1);
        changeWs.textColor.input.borderStyle('solid', 'solid', 'solid', 'solid');
        changeWs.textColor.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
        changeWs.textColor.input.borderRadius(5, 5, 5, 5);
        
        changeWs.textInner.width('100%');
        changeWs.textInner.zIndex(7);
        changeWs.textInner.height('25px');
        changeWs.textInner.marginBottom('25px');

        changeWs.textInner.label.width('50%');
        changeWs.textInner.label.height('25px');
        changeWs.textInner.label.float('left');
        changeWs.textInner.label.innerText('Change the text on head');

        changeWs.textInner.input.type('input');
        changeWs.textInner.input.width('50%');
        changeWs.textInner.input.height('25px');
        changeWs.textInner.input.float('right');
        changeWs.textInner.input.boxSizing('border-box');
        changeWs.textInner.input.borderWidth(1, 1, 1, 1);
        changeWs.textInner.input.borderStyle('solid', 'solid', 'solid', 'solid');
        changeWs.textInner.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
        changeWs.textInner.input.borderRadius(5, 5, 5, 5);

        changeWs.borderBackground.width('100%');
        changeWs.borderBackground.zIndex(7);
        changeWs.borderBackground.height('25px');
        changeWs.borderBackground.marginBottom('25px');

        changeWs.borderBackground.label.width('50%');
        changeWs.borderBackground.label.height('25px');
        changeWs.borderBackground.label.float('left');
        changeWs.borderBackground.label.innerText('Change the border color');

        changeWs.borderBackground.input.type('color');
        changeWs.borderBackground.input.width('50%');
        changeWs.borderBackground.input.height('25px');
        changeWs.borderBackground.input.float('right');
        changeWs.borderBackground.input.boxSizing('border-box');
        changeWs.borderBackground.input.borderWidth(1, 1, 1, 1);
        changeWs.borderBackground.input.borderStyle('solid', 'solid', 'solid', 'solid');
        changeWs.borderBackground.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
        changeWs.borderBackground.input.borderRadius(5, 5, 5, 5);

        changeWs.reset.type('reset');
        changeWs.reset.width('50%');
        changeWs.reset.float('left');
        changeWs.reset.zIndex(7);
        changeWs.reset.height('25px');
        changeWs.reset.innerText('Clear');

        changeWs.submit.type('submit');
        changeWs.submit.float('left');
        changeWs.submit.width('50%');
        changeWs.submit.zIndex(7);
        changeWs.submit.height('25px');
        changeWs.submit.innerText('Apply');
        self.elements.push(changeWs);

        var helpWs = new Redactor({domElement: document.body}, 3);
        self.domElement = helpWs.domElement;
        box.helpWs = helpWs;

        helpWs.hidden(true);
        helpWs.position('absolute');
        helpWs.width(box.width());
        helpWs.height(box.height());
        helpWs.border('5px solid rgba(166, 70, 98, 1)');
        helpWs.background('#788');
        helpWs.zIndex(2);
        helpWs.left('2.5%');
        helpWs.top('2.5%');

        helpWs.head.background('rgba(166, 70, 98, 1)');
        helpWs.head.zIndex(4);
        helpWs.head.textAlign('center');
        helpWs.head.textShadow('1px 2px 2px #000');
        helpWs.head.lineHeight('26px');
        helpWs.head.height('50px');
        helpWs.head.width('100%');
        helpWs.head.cursor('pointer');
        helpWs.head.top('0px');
        helpWs.head.left('0px');

        helpWs.closeBtn.type('button');
        helpWs.closeBtn.right('0');
        helpWs.closeBtn.innerHTML('&#xD7');
        helpWs.closeBtn.border('none');
        helpWs.closeBtn.color('rgb(255, 119, 153)');
        helpWs.closeBtn.background('rgba(166, 70, 98, 1)');
        helpWs.closeBtn.fontSize('25px');
        helpWs.closeBtn.position('absolute');
        helpWs.closeBtn.padding('0,0,0,0');
        helpWs.closeBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        helpWs.closeBtn.zIndex(5);

        helpWs.innerBox.width('100%');
        helpWs.innerBox.height('450px');
        helpWs.innerBox.display('flex');

        helpWs.boxInner.border('3px solid rgba(166, 70, 98, 1)');
        helpWs.boxInner.padding('5px');
        helpWs.boxInner.width('80%');
        helpWs.boxInner.height('auto');
        helpWs.boxInner.zIndex(3);
        helpWs.boxInner.margin('auto');
        helpWs.boxInner.border('5px solid rgba(166, 70, 98, 1)');
        helpWs.boxInner.padding('25px');
        helpWs.boxInner.zIndex(3);
        helpWs.boxInner.margin('auto');

        helpWs.textColor.width('100%');
        helpWs.textColor.zIndex(7);
        helpWs.textColor.height('auto');
        helpWs.textColor.innerHTML('<p style ="height:auto">Create a window with form elements and edit it</p> <p style ="height:auto">Drag and drop the left window</p><p style ="height:auto" >Drag the HTML form elements from the right window to the left window and drag them within the left window</p><p style ="height:auto">Change the size of the left window and all form elements</p><p style ="height:auto">Click the right mouse button and edit the style of the form element</p><p>You can double-click on the form element and delete it</p><p>Delete right window<p/>');
        self.elements.push(helpWs);

        var htmlBox = new Fieldset({domElement: document.body}, 2);
        self.domElement = htmlBox.domElement;
        htmlBox.neigbord = box;
        box.neigbord = htmlBox;

        htmlBox.hidden(storage.htmlBox.hidden);
        htmlBox.position('absolute');
        htmlBox.width('45%');
        htmlBox.height('500px');
        htmlBox.border('5px solid rgb(166, 70, 98)');
        htmlBox.zIndex(1);
        htmlBox.right('2.5%');
        htmlBox.top('2.5%');

        htmlBox.head.height('50px');
        htmlBox.head.width('100%');
        htmlBox.head.webkitUserSelect('none');
        htmlBox.head.background('#a64662');
        htmlBox.head.boxSizing('border-box');

        htmlBox.closeBtn.type('button');
        htmlBox.closeBtn.right("-5px");
        htmlBox.closeBtn.innerHTML('&#xD7');
        htmlBox.closeBtn.color('#dadadf');
        htmlBox.closeBtn.background('rgba(255, 119, 153, 0)');
        htmlBox.closeBtn.fontSize('25px');
        htmlBox.closeBtn.position('absolute');
        htmlBox.closeBtn.padding('0,0,0,0');
        htmlBox.closeBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
        htmlBox.closeBtn.border('0px solid #000');
        htmlBox.closeBtn.zIndex('2');

        htmlBox.innerBox.width('100%');
        htmlBox.innerBox.height('450px');
        htmlBox.innerBox.display('flex');
        htmlBox.innerBox.zIndex(2);

        htmlBox.boxInner.width('80%');
        htmlBox.boxInner.height('350px');
        htmlBox.boxInner.zIndex(2);
        htmlBox.boxInner.margin('auto');

        htmlBox.inputLabel.position('absolute');
        htmlBox.inputLabel.width(htmlBox.boxInner.width());
        htmlBox.inputLabel.height('30px');
        htmlBox.inputLabel.cursor("pointer");
        htmlBox.inputLabel.left('11%');
        htmlBox.inputLabel.top('110px');
        htmlBox.inputLabel.webkitUserSelect('none');

        htmlBox.inputLabel.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.inputLabel.box.boxSizing('border-box');
        htmlBox.inputLabel.box.width('98%');
        htmlBox.inputLabel.box.height('30px');
        htmlBox.inputLabel.box.float('left');
        htmlBox.inputLabel.box.background('#fff');
        htmlBox.inputLabel.box.cursor('pointer');

        htmlBox.inputLabel.resizeBox.width('2%');
        htmlBox.inputLabel.resizeBox.height('30px');
        htmlBox.inputLabel.resizeBox.cursor('e-resize');
        htmlBox.inputLabel.resizeBox.float('right');

        htmlBox.inputLabel.label.innerText('Enter Form Data');
        htmlBox.inputLabel.label.fontSize('16px');
        htmlBox.inputLabel.label.width('49%');
        htmlBox.inputLabel.label.height('28px');
        htmlBox.inputLabel.label.display('block');
        htmlBox.inputLabel.label.float('left');
        htmlBox.inputLabel.label.textAlign('center');
        htmlBox.inputLabel.label.lineHeight('25px');

        htmlBox.inputLabel.input.width('49%');
        htmlBox.inputLabel.input.height('28px');
        htmlBox.inputLabel.input.float('right');
        htmlBox.inputLabel.input.display('block');
        htmlBox.inputLabel.input.boxSizing('border-box');
        htmlBox.inputLabel.input.background('#fff');

        htmlBox.select.position('absolute');
        htmlBox.select.cursor("pointer");
        htmlBox.select.height('30px');
        htmlBox.select.width(htmlBox.boxInner.width());
        htmlBox.select.left('11%');
        htmlBox.select.top('270px');
        htmlBox.select.webkitUserSelect('none');

        htmlBox.select.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.select.box.boxSizing('border-box');
        htmlBox.select.box.width('98%');
        htmlBox.select.box.height('30px');
        htmlBox.select.box.float('left');
        htmlBox.select.box.background('#fff');
        htmlBox.select.box.cursor('pointer');

        htmlBox.select.resizeBox.width('2%');
        htmlBox.select.resizeBox.height('30px');
        htmlBox.select.resizeBox.cursor('e-resize');
        htmlBox.select.resizeBox.float('right');

        htmlBox.select.select.width('100%');
        htmlBox.select.select.float('right');
        htmlBox.select.select.display('block');
        htmlBox.select.select.pointerEvents('none');
        htmlBox.select.select.height('28px');
        htmlBox.select.select.float('right');

        htmlBox.input.position('absolute');
        htmlBox.input.width(htmlBox.boxInner.width());
        htmlBox.input.height('30px');
        htmlBox.input.top('165px');
        htmlBox.input.left('11%');
        htmlBox.input.cursor("pointer");
        htmlBox.input.zIndex(2);
        htmlBox.input.webkitUserSelect('none');

        htmlBox.input.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.input.box.boxSizing('border-box');
        htmlBox.input.box.width('98%');
        htmlBox.input.box.height('30px');
        htmlBox.input.box.cursor('pointer');
        htmlBox.input.box.float('left');
        htmlBox.input.box.background('#fff');

        htmlBox.input.resizeBox.width('2%');
        htmlBox.input.resizeBox.height('25px');
        htmlBox.input.resizeBox.cursor('e-resize');
        htmlBox.input.resizeBox.float('right');

        htmlBox.input.input.height('28px');
        htmlBox.input.input.background('#fff');
        htmlBox.input.input.width('100%');
        htmlBox.input.input.placeholder('Enter Form Data');
        htmlBox.input.input.color('#297');
        htmlBox.input.input.boxSizing('border-box');

        htmlBox.label.position('absolute');
        htmlBox.label.width(htmlBox.boxInner.width());
        htmlBox.label.height('30px');
        htmlBox.label.top('220px');
        htmlBox.label.left('11%');
        htmlBox.label.cursor("pointer");
        htmlBox.label.zIndex(2);
        htmlBox.label.webkitUserSelect('none');

        htmlBox.label.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.label.box.float('left');
        htmlBox.label.box.display('block');
        htmlBox.label.box.height('30px');
        htmlBox.label.box.boxSizing('border-box');
        htmlBox.label.box.background('#fff');
        htmlBox.label.box.width('98%');
        htmlBox.label.box.cursor('pointer');

        htmlBox.label.resizeBox.width('2%');
        htmlBox.label.resizeBox.height('25px');
        htmlBox.label.resizeBox.cursor('e-resize');
        htmlBox.label.resizeBox.float('right');

        htmlBox.label.label.display('block');
        htmlBox.label.label.height('28px');
        htmlBox.label.label.width('100%');
        htmlBox.label.label.color('#000');
        htmlBox.label.label.innerText('Enter Form Data');
        htmlBox.label.label.lineHeight('25px');
        htmlBox.label.label.textAlign('center');

        htmlBox.checkbox.width(htmlBox.boxInner.width());
        htmlBox.checkbox.height('30px');
        htmlBox.checkbox.position('absolute');
        htmlBox.checkbox.top('330px');
        htmlBox.checkbox.webkitUserSelect('none');
        htmlBox.checkbox.left('11%');
        htmlBox.checkbox.boxSizing('border-box');

        htmlBox.checkbox.box.float('left');
        htmlBox.checkbox.box.height('100%');
        htmlBox.checkbox.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.checkbox.box.width('98%');
        htmlBox.checkbox.box.background('#fff');
        htmlBox.checkbox.box.display('flex');
        htmlBox.checkbox.box.boxSizing('border-box');

        htmlBox.checkbox.label.innerText('Enter Form Data');
        htmlBox.checkbox.label.fontSize('16px');
        htmlBox.checkbox.label.width('50%');
        htmlBox.checkbox.label.height('100%');
        htmlBox.checkbox.label.display('block');
        htmlBox.checkbox.label.float('left');
        htmlBox.checkbox.label.textAlign('center');
        htmlBox.checkbox.label.lineHeight('25px');

        htmlBox.checkbox.checkbox.border('none');
        htmlBox.checkbox.checkbox.height('25px');
        htmlBox.checkbox.checkbox.width('25px');
        htmlBox.checkbox.checkbox.float('left');
        htmlBox.checkbox.checkbox.marginTop('2px');

        htmlBox.checkbox.resizeBox.width('2%');
        htmlBox.checkbox.resizeBox.height('30px');
        htmlBox.checkbox.resizeBox.cursor('e-resize');
        htmlBox.checkbox.resizeBox.float('right');

        htmlBox.textarea.width(htmlBox.boxInner.width());
        htmlBox.textarea.height('50px');
        htmlBox.textarea.box.border('1px solid rgb(166, 70, 98)');
        htmlBox.textarea.position('absolute');
        htmlBox.textarea.top('390px');
        htmlBox.textarea.left('11%');
        htmlBox.textarea.webkitUserSelect('none');

        htmlBox.textarea.box.float('left');
        htmlBox.textarea.box.boxSizing('border-box');
        htmlBox.textarea.box.display('block');
        htmlBox.textarea.box.height('50px');
        htmlBox.textarea.box.width('98%');
        htmlBox.textarea.box.cursor('pointer');
        htmlBox.textarea.box.background('#fff');

        htmlBox.textarea.resizeBox.width('2%');
        htmlBox.textarea.resizeBox.height('50px');
        htmlBox.textarea.resizeBox.cursor('e-resize');
        htmlBox.textarea.resizeBox.float('right');

        htmlBox.textarea.textarea.height('42px');
        htmlBox.textarea.textarea.display('block');
        htmlBox.textarea.textarea.border('none');
        htmlBox.textarea.textarea.background('#fff');
        htmlBox.textarea.textarea.width('98.79%');
        htmlBox.textarea.textarea.resize('none');
        htmlBox.textarea.textarea.placeholder('Enter Message');
        self.elements.push(htmlBox);

        var hideBox = new Redactor({domElement: document.body}, 4);
        self.domElement = hideBox.domElement;
        box.hideBox = hideBox;
        hideBox.hidden(storage.hideBox.hidden);
        hideBox.position('absolute');
        hideBox.width('25px');
        hideBox.height('150px');
        hideBox.background(storage.hideBox.background);
        hideBox.right('0');
        hideBox.bottom('50%');
        hideBox.border(storage.hideBox.border);
        hideBox.boxSizing('border-box');
        self.elements.push(hideBox);

    };

    draw();

};

