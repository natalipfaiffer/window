var Label = function (target) {
    Basestyle.apply(this, arguments);
    Label.prototype = Object.create(Basestyle.prototype);
    Label.prototype.constructor = Label;
    
    var self = this;

    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    }

    var draw = function () {
        var label = document.createElement('label');
        self.label = label;
        self.domElement = label;
        target.domElement.appendChild(label);
    };

    draw();

};

Label.prototype.style = {};


