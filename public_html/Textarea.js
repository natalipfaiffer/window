var Textarea = function (target) {
    Basestyle.apply(this, arguments);
    Textarea.prototype = Object.create(Basestyle.prototype);
    Textarea.prototype.constructor = Textarea;

    var self = this;
    this.domElement;

    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    };

    var draw = function () {
        var textarea = document.createElement('textarea');
        var styleElem = document.head.appendChild(document.createElement("style"));
        self.styleElem = styleElem;
        self.textarea = textarea;
        self.domElement = textarea;
        this.id = getRandomId(textarea.constructor.name);
        textarea.id = this.id;
        target.domElement.appendChild(textarea);
    };
    
    draw();
    
};
Textarea.prototype.style = {};