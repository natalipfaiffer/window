var Form = function (target, array, mode, windMode) {
    Basestyle.apply(this, arguments);
    Form.prototype = Object.create(Basestyle.prototype);
    Form.prototype.constructor = Form;

    this.domElement;
    var self = this;
    this.statMode = mode;
    this.elements = [];
    this.id = getRandomId('Form');
    this.windMode = windMode;

    this.draw = function () {
        switch (this.mode()) {
            case 1:
                this.drawInputLabelMode();
                break;
            case 2:
                this.drawSelectMode();
                break;
            case 3:
                this.drawCheckboxMode();
                break;
            case 4:
                this.drawInputMode();
                break;
            case 5:
                this.drawLabelMode();
                break;
            case 6:
                this.drawTextareaMode();
                break;
        }

        var minWidth = 250;
        var dx, dy;
        var resizeElement = self.form;
        var move = self.box;
        var resize = self.resizeBox;
        resizeElement.moveFlag = false;
        resizeElement.resizeFlag = false;
        resizeElement.id = id;

        var shiftX;
        var shiftY;
        var fixLeft;
        var fixTop;
        var fixWidth;
        var fixHeight;

        if (windMode) {
            move.domElement.addEventListener("mousedown", function (e) {
                resizeElement.moveFlag = true;
                fixLeft = resizeElement.domElement.offsetLeft;
                fixTop = resizeElement.domElement.offsetTop;
                dy = e.pageY;
                dx = e.pageX;
                shiftX = dx - fixLeft;
                shiftY = dy - fixTop;
                moveAt(e);
            });

            function moveAt(e) {

                resizeElement.moveFlag = true;
                if ((e.pageX - shiftX) > 0 && (self.parent.clientWidth + 10) > (e.pageX - shiftX) + parseInt(resizeElement.width())) {
                    resizeElement.left((e.pageX - shiftX) + "px");
                    storage.childs[self.id].dragElement.left = resizeElement.left();
                }
                if ((e.pageY - shiftY) > 0 && (self.parent.clientHeight) > (e.pageY - shiftY) + parseInt(resizeElement.height())) {
                    resizeElement.top((e.pageY - shiftY) + "px");
                    storage.childs[self.id].dragElement.top = resizeElement.top();
                }
            }

            this.id = getRandomId(move.constructor.name);
            move.id = this.id;

            var onmousemove = function (em) {
                if (resizeElement.moveFlag) {
                    moveAt(em);
                }
            };

            var onmouseup = function () {
                resizeElement.moveFlag = false;
            };

            onmouseupCallback[this.id] = onmouseup;
            mousemoveCallback[this.id] = onmousemove;

        }

        resizeElement.domElement.addEventListener("contextmenu", function (e) {
            resizeElement.moveFlag = false;
            e.preventDefault();
            e.stopPropagation();
            fixLeft = self.parent.getBoundingClientRect().left;
            fixTop = self.parent.getBoundingClientRect().top;
            fixWidth = self.parent.clientWidth;
            fixHeight = self.parent.clientHeight;

            var rdctr = new Redactor({domElement: document.body}, 2);
            rdctr.onClick = function () {
                var parent = self;
                if (parent.select) {

                    var string = rdctr.innerTextLabel.input.value();
                    var splitString = string.split(',');
                    var select = self.select.domElement;
                    var i;

                    for (i = select.options.length - 1; i >= 0; i--)
                    {
                        select.remove(i);
                    }

                    for (var i in splitString) {
                        var option = document.createElement("option");
                        option.text = splitString[i];
                        select.options.add(option, i);
                    }

                }

                var placeholderCol = (rdctr.innerTextLabel.input.value());
                var clrTxt = (rdctr.colorText.input.value());

                parent.box && parent.box.background(rdctr.backgroundForm.input.value());
                parent.box && parent.box.border('1px solid' + rdctr.borderColor.input.value());
                parent.input && parent.input.background(rdctr.backgroundForm.input.value());

                if (self.label && self.input) {
                    parent.label && parent.label.innerText(rdctr.innerTextLabel.input.value());
                    parent.label && parent.label.color(rdctr.colorText.input.value());
                    parent.input && parent.input.color(rdctr.colorText.input.value());
                } else if (self.input) {
                    var inp = document.getElementById(self.input.input.id);
                    parent.input.styleElem.innerHTML = "#" + self.input.input.id + "::placeholder {color: " + clrTxt + "}";
                    inp.setAttribute("placeholder", placeholderCol);
                } else if (self.textarea) {
                    var text = document.getElementById(self.textarea.textarea.id);
                    parent.textarea.styleElem.innerHTML = "#" + self.textarea.textarea.id + "::placeholder {color: " + clrTxt + "}";
                    text.setAttribute("placeholder", placeholderCol);
                } else if (self.label) {
                    parent.label && parent.label.color(rdctr.colorText.input.value());
                    parent.label && parent.label.innerText(rdctr.innerTextLabel.input.value());
                }

                parent.checkbox && parent.checkbox.innerText(rdctr.innerTextLabel.input.value());
                parent.select && parent.select.background(rdctr.backgroundForm.input.value());
                parent.checkbox && parent.checkbox.marginTop();
                parent.textarea && parent.textarea.background(rdctr.backgroundForm.input.value());
                parent.select && parent.select.color(rdctr.colorText.input.value());

                var optionItems = [];
                for (var i in splitString) {
                    optionItems.push({
                        text: splitString[i],
                        value: i,
                    })
                }

                storage.childs[self.id].dragElement.array = optionItems;
                storage.childs[self.id].dragElement.boxBorder = ('1px solid' + rdctr.borderColor.input.value());
                storage.childs[self.id].dragElement.boxBackground = (rdctr.backgroundForm.input.value());
                storage.childs[self.id].dragElement.inputBackground = (rdctr.backgroundForm.input.value());
                storage.childs[self.id].dragElement.labelColor = (rdctr.colorText.input.value());
                storage.childs[self.id].dragElement.labelInnerText = (rdctr.innerTextLabel.input.value());
                storage.childs[self.id].dragElement.inputColor = (rdctr.colorText.input.value());
                storage.childs[self.id].dragElement.selectBackground = (rdctr.backgroundForm.input.value());
                storage.childs[self.id].dragElement.textareaBackground = (rdctr.backgroundForm.input.value());
                storage.childs[self.id].dragElement.textareaPlaceholder = (rdctr.innerTextLabel.input.value());
                storage.childs[self.id].dragElement.selectColor = (rdctr.colorText.input.value());

                if (self.input && self.label) {
                    storage.childs[self.id].dragElement.labelInnerText = (rdctr.innerTextLabel.input.value());
                } else if (self.input) {
                    storage.childs[self.id].dragElement.inputPlaceholder = (rdctr.innerTextLabel.input.value());
                    storage.childs[self.id].dragElement.inputPlaceholderColor = "#" + self.input.input.id + "::placeholder {color: " + clrTxt + "}";
                } else if (self.textarea) {
                    storage.childs[self.id].dragElement.placeholderColorTextarea = "#" + self.textarea.textarea.id + "::placeholder {color: " + clrTxt + "}"
                }
            };


            rdctr.position('absolute');
            rdctr.width(fixWidth + "px");
            rdctr.height(fixHeight + "px");
            rdctr.border('5px solid rgba(166, 70, 98, 1)');
            rdctr.background('#fff');
            rdctr.zIndex(2);
            rdctr.left(fixLeft + "px");
            rdctr.top(fixTop + "px");

            rdctr.head.background('rgba(166, 70, 98, 1)');
            rdctr.head.zIndex(4);
            rdctr.head.textAlign('center');
            rdctr.head.textShadow('1px 2px 2px #000');
            rdctr.head.lineHeight('26px');
            rdctr.head.height('50px');
            rdctr.head.width('100%');
            rdctr.head.cursor('pointer');
            rdctr.head.top('0px');
            rdctr.head.left('0px');

            rdctr.closeBtn.type('button');
            rdctr.closeBtn.right('0');
            rdctr.closeBtn.innerHTML('&#xD7');
            rdctr.closeBtn.border('none');
            rdctr.closeBtn.color('rgb(255, 119, 153)');
            rdctr.closeBtn.background('rgba(166, 70, 98, 1)');
            rdctr.closeBtn.fontSize('25px');
            rdctr.closeBtn.position('absolute');
            rdctr.closeBtn.padding('0,0,0,0');
            rdctr.closeBtn.textShadow('rgb(0, 0, 0) 1px 2px 2px');
            rdctr.closeBtn.zIndex(5);

            rdctr.innerBox.width('100%');
            rdctr.innerBox.height('450px');
            rdctr.innerBox.display('flex');

            rdctr.boxInner.border('3px solid rgba(166, 70, 98, 1)');
            rdctr.boxInner.width('70%');
            rdctr.boxInner.height('auto');
            rdctr.boxInner.zIndex(3);
            rdctr.boxInner.margin('auto');
            rdctr.boxInner.padding('25px');

            rdctr.innerTextLabel.width('100%');
            rdctr.innerTextLabel.zIndex(7);
            rdctr.innerTextLabel.height('25px');
            rdctr.innerTextLabel.marginBottom('25px');

            rdctr.innerTextLabel.label.width('50%');
            rdctr.innerTextLabel.label.height('25px');
            rdctr.innerTextLabel.label.float('left');
            rdctr.innerTextLabel.label.innerText('Enter text label');

            rdctr.innerTextLabel.input.type('text');
            rdctr.innerTextLabel.input.width('50%');
            rdctr.innerTextLabel.input.height('25px');
            rdctr.innerTextLabel.input.float('right');
            rdctr.innerTextLabel.input.boxSizing('border-box');
            rdctr.innerTextLabel.input.borderWidth(1, 1, 1, 1);
            rdctr.innerTextLabel.input.borderStyle('solid', 'solid', 'solid', 'solid');
            rdctr.innerTextLabel.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
            rdctr.innerTextLabel.input.borderRadius(5, 5, 5, 5);

            rdctr.borderColor.width('100%');
            rdctr.borderColor.zIndex(7);
            rdctr.borderColor.height('25px');
            rdctr.borderColor.marginBottom('25px');

            rdctr.borderColor.label.width('50%');
            rdctr.borderColor.label.height('25px');
            rdctr.borderColor.label.float('left');

            rdctr.borderColor.input.type('color');
            rdctr.borderColor.input.width('50%');
            rdctr.borderColor.input.height('25px');
            rdctr.borderColor.input.float('right');
            rdctr.borderColor.input.boxSizing('border-box');
            rdctr.borderColor.input.borderWidth(1, 1, 1, 1);
            rdctr.borderColor.input.borderStyle('solid', 'solid', 'solid', 'solid');
            rdctr.borderColor.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
            rdctr.borderColor.input.borderRadius(5, 5, 5, 5);

            rdctr.borderColor.label.innerText('Change the border color');
            rdctr.borderColor.label.width('50%');
            rdctr.borderColor.label.height('25px');
            rdctr.borderColor.label.float('left');

            rdctr.backgroundForm.width('100%');
            rdctr.backgroundForm.zIndex(7);
            rdctr.backgroundForm.height('25px');
            rdctr.backgroundForm.marginBottom('25px');

            rdctr.backgroundForm.label.width('50%');
            rdctr.backgroundForm.label.height('25px');
            rdctr.backgroundForm.label.float('left');
            rdctr.backgroundForm.label.innerText('Change the background color');

            rdctr.backgroundForm.input.type('color');
            rdctr.backgroundForm.input.width('50%');
            rdctr.backgroundForm.input.height('25px');
            rdctr.backgroundForm.input.float('right');
            rdctr.backgroundForm.input.boxSizing('border-box');
            rdctr.backgroundForm.input.borderWidth(1, 1, 1, 1);
            rdctr.backgroundForm.input.borderStyle('solid', 'solid', 'solid', 'solid');
            rdctr.backgroundForm.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
            rdctr.backgroundForm.input.borderRadius(5, 5, 5, 5);

            rdctr.colorText.width('100%');
            rdctr.colorText.zIndex(7);
            rdctr.colorText.height('25px');
            rdctr.colorText.marginBottom('25px');

            rdctr.colorText.label.width('50%');
            rdctr.colorText.label.height('25px');
            rdctr.colorText.label.float('left');
            rdctr.colorText.label.innerText('Change the text color');

            rdctr.colorText.input.type('color');
            rdctr.colorText.input.width('50%');
            rdctr.colorText.input.height('25px');
            rdctr.colorText.input.float('right');
            rdctr.colorText.input.boxSizing('border-box');
            rdctr.colorText.input.borderWidth(1, 1, 1, 1);
            rdctr.colorText.input.borderStyle('solid', 'solid', 'solid', 'solid');
            rdctr.colorText.input.borderColor('#C0C0C0', '#C0C0C0', '#C0C0C0', '#C0C0C0');
            rdctr.colorText.input.borderRadius(5, 5, 5, 5);

            rdctr.boxButton.width('100%');
            rdctr.boxButton.height('25px');
            rdctr.boxButton.zIndex(3);

            rdctr.resetForm.type('reset');
            rdctr.resetForm.width('50%');
            rdctr.resetForm.float('left');
            rdctr.resetForm.zIndex(7);
            rdctr.resetForm.height('25px');
            rdctr.resetForm.innerText('Clear');

            rdctr.submit.type('submit');
            rdctr.submit.float('left');
            rdctr.submit.width('50%');
            rdctr.submit.zIndex(7);
            rdctr.submit.height('25px');
            rdctr.submit.innerText('Apply');

            self.elements.push(rdctr);
        });

        if (windMode) {
            resizeElement.domElement.addEventListener("dblclick", function () {
                resizeElement.moveFlag = false;
                this.parentNode.removeChild(this);
                delete storage.childs[self.id];
            })
        }

        var calcWidth;

        if (windMode) {
            resize.domElement.addEventListener("mousedown", function (e) {
                resizeElement.resizeFlag = true;
                dx = e.pageX;
                resize(e);
            });
        }

        var resize = function (em) {
            resizeElement.resizeFlag = true;
            if (resizeElement.resizeFlag) {
                var deltaX = (em.pageX - dx);
                var widthElement = self.form.domElement.offsetWidth;
                calcWidth = widthElement + deltaX;
                resizeElement.width(parseInt(calcWidth >= minWidth ? calcWidth : minWidth) + "px");
                dx = em.pageX;
                storage.childs[self.id].dragElement.width = resizeElement.width();
            } else {
                resizeElement.width(calcWidth + "px");
                resizeElement.resizeFlag = false;
            }

        };

        this.id = getRandomId(resize.constructor.name);
        resize.id = this.id;

        var onmousemove = function (em) {
            if (resizeElement.resizeFlag) {
                resize(em);
            }
        };
        var onmouseup = function () {
            if (windMode) {
                resizeElement.resizeFlag = false;
            }

        };
        onmouseupCallback[this.id] = onmouseup;
        mousemoveCallback[this.id] = onmousemove;
    };

    this.mode = function (value) {
        if (value !== undefined) {
            this.statMode = value;
            this.draw();
        } else {
            return parseInt(this.statMode);
        }
    };

    this.drawInputLabelMode = function () {
        this.objType = "InputLabel";
        var form = new Box(target);
        form.mode = 1;
        self.form = form;
        form.domElement.id = self.id;
        self.domElement = form.domElement;
        var box = new Box(form);
        self.box = box;
        var label = new Label(box);
        self.label = label;
        var input = new Input(box);
        self.input = input;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.drawSelectMode = function () {
        this.objType = "Select";
        var form = new Box(target);
        form.mode = 2;
        self.form = form;
        form.domElement.id = self.id;
        self.domElement = form.domElement;
        var box = new Box(form);
        self.box = box;
        var select = new Select(box, array);
        self.select = select;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.drawCheckboxMode = function () {
        this.objType = "Checkbox";
        var form = new Box(target);
        form.mode = 3;
        self.form = form;
        form.domElement.id = self.id;
        self.domElement = form.domElement;
        var box = new Box(form);
        self.box = box;
        var label = new Label(box);
        self.label = label;
        var checkbox = new Checkbox(box);
        self.checkbox = checkbox;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.drawInputMode = function () {
        this.objType = "Input";
        var form = new Box(target);
        form.mode = 4;
        self.form = form;
        self.domElement = form.domElement;
        form.domElement.id = self.id;
        var box = new Box(form);
        self.box = box;
        var input = new Input(box);
        self.input = input;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.drawLabelMode = function () {
        this.objType = "Label";
        var form = new Box(target);
        form.mode = 5;
        self.form = form;
        form.domElement.id = self.id;
        self.domElement = form.domElement;
        var box = new Box(form);
        self.box = box;
        var label = new Label(box);
        self.label = label;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.drawTextareaMode = function () {
        this.objType = "Textarea";
        var form = new Box(target);
        form.mode = 6;
        self.form = form;
        form.domElement.id = self.id;
        self.domElement = form.domElement;
        var box = new Box(form);
        self.box = box;
        var textarea = new Textarea(box);
        self.textarea = textarea;
        var resizeBox = new Box(form);
        self.resizeBox = resizeBox;
    };

    this.draw();

};


