var Select = function (target, array) {
    Basestyle.apply(this, arguments);
    Select.prototype = Object.create(Basestyle.prototype);
    Select.prototype.constructor = Select;

    var self = this;
    this.domElement;

    var draw = function () {
        var select = document.createElement("select");
        self.select = select;
        self.select.disabled;
        self.domElement = select;
        target.domElement.appendChild(select);

        var option = document.createElement("option");
        self.option = option;
        option.value = 0;
        option.setAttribute('selected', 'selected');
        self.option = option;
        option.select = self;
        option.innerText = "Select";
        select.appendChild(option);

        for (var i in array) {
            var option = document.createElement("option");
            option.value = array[i].value;
            self.option = option;
            option.select = self;
            option.innerText = array[i].text;
            select.appendChild(option);
        }
    };

    draw();

};

Select.prototype.style = {};