var Basestyle = function () {
    this.domElement;
    var self = this;
    var fieldsetFlag = false;

    this.visible = function () {
        if (fieldsetFlag) {
            self.domElement.style.visibility = 'hidden';
            fieldsetFlag = false;
        } else {
            self.domElement.style.visibility = 'visible';
            fieldsetFlag = true;
        }
    };

    this.addElement = function (element) {
        self.domElement.appendChild(element.domContainer);
    };

    this.alignContent = function (value) {
        if (value !== undefined) {
            self.domElement.style.alignContent = value;
        } else {
            return parseInt(self.domElement.style.alignContent);
        }
    };

    this.alignSelf = function (value) {
        if (value !== undefined) {
            self.domElement.style.alignSelf = value;
        } else {
            return parseInt(self.domElement.style.alignSelf);
        }
    };

    this.alignItems = function (value) {
        if (value !== undefined) {
            self.domElement.style.alignItems = value;
        } else {
            return parseInt(self.domElement.style.alignItems);
        }
    };

    this.animation = function (value) {
        if (value !== undefined) {
            self.domElement.style.animation = value;
        } else {
            return parseInt(self.domElement.style.animation);
        }
    };

    this.animationDelay = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationDelay = value;
        } else {
            return parseInt(self.domElement.style.animationDelay);
        }
    };

    this.animationDirection = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationDirection = value;
        } else {
            return parseInt(self.domElement.style.animationDirection);
        }
    };
    this.animationDuration = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationDuration = value;
        } else {
            return parseInt(self.domElement.style.animationDuration);
        }
    };

    this.animationFillMode = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationFillMode = value;
        } else {
            return parseInt(self.domElement.style.animationFillMode);
        }
    };
    this.animationIterationCount = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationIterationCount = value;
        } else {
            return parseInt(self.domElement.style.animationIterationCount);
        }
    };

    this.animationName = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationName = value;
        } else {
            return parseInt(self.domElement.style.animationName);
        }
    };

    this.animationPlayState = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationPlayState = value;
        } else {
            return parseInt(self.domElement.style.animationPlayState);
        }
    };

    this.animationTimingFunction = function (value) {
        if (value !== undefined) {
            self.domElement.style.animationTimingFunction = value;
        } else {
            return parseInt(self.domElement.style.animationTimingFunction);
        }
    };

    this.backfaceVisibility = function (value) {
        if (value !== undefined) {
            self.domElement.style.backfaceVisibility = value;
        } else {
            return (self.domElement.style.backfaceVisibility);
        }
    };

    this.background = function (value) {
        if (value !== undefined) {
            self.domElement.style.background = value;
        } else {
            return (self.domElement.style.background);
        }
    };

    this.backgroundAttachment = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundAttachment = value;
        } else {
            return (self.domElement.style.backgroundAttachment);
        }
    };
    this.backgroundClip = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundClip = value;
        } else {
            return parseInt(self.domElement.style.backgroundClip);
        }
    };

    this.backgroundColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundColor = value;
        } else {
            return (self.domElement.style.backgroundColor);
        }
    };

    this.backgroundImage = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundImage = value;
        } else {
            return parseInt(self.domElement.style.backgroundImage);
        }
    };

    this.backgroundOrigin = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundOrigin = value;
        } else {
            return parseInt(self.domElement.style.backgroundOrigin);
        }
    };
    this.backgroundPosition = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundPosition = value;
        } else {
            return parseInt(self.domElement.style.backgroundPosition);
        }
    };

    this.backgroundRepeat = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundRepeat = value;
        } else {
            return parseInt(self.domElement.style.backgroundRepeat);
        }
    };
    this.backgroundSize = function (value) {
        if (value !== undefined) {
            self.domElement.style.backgroundSize = value;
        } else {
            return parseInt(self.domElement.style.backgroundSize);
        }
    };

    this.border = function (value) {
        if (value !== undefined) {
            self.domElement.style.border = value;
        } else {
            return (self.domElement.style.border);
        }
    };
    this.borderBottom = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottom = value;
        } else {
            return parseInt(self.domElement.style.borderBottom);
        }
    };

    this.borderBottomColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottomColor = value;
        } else {
            return parseInt(self.domElement.style.borderBottomColor);
        }
    };
    this.borderBottomLeftRadius = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottomLeftRadius = value;
        } else {
            return parseInt(self.domElement.style.borderBottomLeftRadius);
        }
    };

    this.borderBottomRightRadius = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottomRightRadius = value;
        } else {
            return parseInt(self.domElement.style.borderBottomRightRadius);
        }
    };

    this.borderBottomStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottomStyle = value;
        } else {
            return parseInt(self.domElement.style.borderBottomStyle);
        }
    };

    this.borderBottomWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderBottomWidth = value;
        } else {
            return parseInt(self.domElement.style.borderBottomWidth);
        }
    };
    this.borderCollapse = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderCollapse = value;
        } else {
            return parseInt(self.domElement.style.borderCollapse);
        }
    };

    this.borderColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderColor = value;
        } else {
            return parseInt(self.domElement.style.borderColor);
        }
    };

    this.borderImage = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderImage = value;
        } else {
            return parseInt(self.domElement.style.borderImage);
        }
    };

    this.borderLeft = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderLeft = value;
        } else {
            return parseInt(self.domElement.style.borderLeft);
        }
    };

    this.borderStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderStyle = value;
        } else {
            return parseInt(self.domElement.style.borderStyle);
        }
    };

    this.borderLeftColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderLeftColor = value;
        } else {
            return parseInt(self.domElement.style.borderLeftColor);
        }
    };

    this.borderLeftStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderLeftStyle = value;
        } else {
            return parseInt(self.domElement.style.borderLeftStyle);
        }
    };

    this.borderLeftWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderLeftWidth = value;
        } else {
            return parseInt(self.domElement.style.borderLeftWidth);
        }
    };

    this.borderRadius = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderRadius = value;
        } else {
            return parseInt(self.domElement.style.borderRadius);
        }
    };

    this.borderRightColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderRightColor = value;
        } else {
            return parseInt(self.domElement.style.borderRightColor);
        }
    };

    this.borderRight = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderRight = value;
        } else {
            return parseInt(self.domElement.style.borderRight);
        }
    };

    this.borderRightStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderRightStyle = value;
        } else {
            return parseInt(self.domElement.style.borderRightStyle);
        }
    };

    this.borderSpacing = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderSpacing = value;
        } else {
            return parseInt(self.domElement.style.borderSpacing);
        }
    };

    this.borderTop = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTop = value;
        } else {
            return parseInt(self.domElement.style.borderTop);
        }
    };

    this.borderTopColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTopColor = value;
        } else {
            return parseInt(self.domElement.style.borderTopColor);
        }
    };

    this.borderTopLeftRadius = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTopLeftRadius = value;
        } else {
            return parseInt(self.domElement.style.borderTopLeftRadius);
        }
    };

    this.borderTopRightRadius = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTopRightRadius = value;
        } else {
            return parseInt(self.domElement.style.borderTopRightRadius);
        }
    };

    this.borderTopStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTopStyle = value;
        } else {
            return parseInt(self.domElement.style.borderTopStyle);
        }
    };

    this.borderTopWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderTopWidth = value;
        } else {
            return parseInt(self.domElement.style.borderTopWidth);
        }
    };

    this.borderWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.borderWidth = value;
        } else {
            return parseInt(self.domElement.style.borderWidth);
        }
    };

    this.bottom = function (value) {
        if (value !== undefined) {
            self.domElement.style.bottom = value;
        } else {
            return (parseInt(self.domElement.style.bottom) || self.domElement.getBoundingClientRect().bottom);
        }
    };

    this.boxShadow = function (value) {
        if (value !== undefined) {
            self.domElement.style.boxShadow = value;
        } else {
            return parseInt(self.domElement.style.boxShadow);
        }
    };

    this.boxSizing = function (value) {
        if (value !== undefined) {
            self.domElement.style.boxSizing = value;
        } else {
            return (self.domElement.style.boxSizing);
        }
    };

    this.captionSide = function (value) {
        if (value !== undefined) {
            self.domElement.style.captionSide = value;
        } else {
            return parseInt(self.domElement.style.captionSide);
        }
    };

    this.clear = function (value) {
        if (value !== undefined) {
            self.domElement.style.clear = value;
        } else {
            return parseInt(self.domElement.style.clear);
        }
    };

    this.color = function (value) {
        if (value !== undefined) {
            self.domElement.style.color = value;
        } else {
            return parseInt(self.domElement.style.color);
        }
    };

    this.columnCount = function (value) {
        if (value !== undefined) {
            self.domElement.style.columnCount = value;
        } else {
            return parseInt(self.domElement.style.columnCount);
        }
    };

    this.clip = function (value) {
        if (value !== undefined) {
            self.domElement.style.clip = value;
        } else {
            return parseInt(self.domElement.style.clip);
        }
    };

    this.content = function (value) {
        if (value !== undefined) {
            self.domElement.style.content = value;
        } else {
            return parseInt(self.domElement.style.content);
        }
    };

    this.counterIncrement = function (value) {
        if (value !== undefined) {
            self.domElement.style.counterIncrement = value;
        } else {
            return parseInt(self.domElement.style.counterIncrement);
        }
    };

    this.counterReset = function (value) {
        if (value !== undefined) {
            self.domElement.style.counterReset = value;
        } else {
            return parseInt(self.domElement.style.counterReset);
        }
    };

    this.cursor = function (value) {
        if (value !== undefined) {
            self.domElement.style.cursor = value;
        } else {
            return (self.domElement.style.cursor);
        }
    };

    this.display = function (value) {
        if (value !== undefined) {
            self.domElement.style.display = value;
        } else {
            return (self.domElement.style.display);
        }
    };

    this.disabled = function (value) {
        if (value !== undefined) {
            self.domElement.disabled = value;
        } else {
            return (self.domElement.disabled);
        }
    };

    this.emptyCells = function (value) {
        if (value !== undefined) {
            self.domElement.style.emptyCells = value;
        } else {
            return parseInt(self.domElement.style.emptyCells);
        }
    };

    this.flex = function (value) {
        if (value !== undefined) {
            self.domElement.style.flex = value;
        } else {
            return parseInt(self.domElement.style.flex);
        }
    };

    this.flexBasis = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexBasis = value;
        } else {
            return parseInt(self.domElement.style.flexBasis);
        }
    };

    this.flexDirection = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexDirection = value;
        } else {
            return parseInt(self.domElement.style.flexDirection);
        }
    };

    this.flexFlow = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexFlow = value;
        } else {
            return parseInt(self.domElement.style.flexFlow);
        }
    };

    this.flexGrow = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexGrow = value;
        } else {
            return parseInt(self.domElement.style.flexGrow);
        }
    };

    this.font = function (value) {
        if (value !== undefined) {
            self.domElement.style.font = value;
        } else {
            return parseInt(self.domElement.style.font);
        }
    };

    this.float = function (value) {
        if (value !== undefined) {
            self.domElement.style.float = value;
        } else {
            return (self.domElement.style.float);
        }
    };

    this.flexWrap = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexWrap = value;
        } else {
            return parseInt(self.domElement.style.flexWrap);
        }
    };

    this.flexShrink = function (value) {
        if (value !== undefined) {
            self.domElement.style.flexShrink = value;
        } else {
            return parseInt(self.domElement.style.flexShrink);
        }
    };

    this.fontFamily = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontFamily = value;
        } else {
            return parseInt(self.domElement.style.fontFamily);
        }
    };

    this.fontKerning = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontKerning = value;
        } else {
            return parseInt(self.domElement.style.fontKerning);
        }
    };

    this.fontSize = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontSize = value;
        } else {
            return (self.domElement.style.fontSize);
        }
    };

    this.fontStretch = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontStretch = value;
        } else {
            return parseInt(self.domElement.style.fontStretch);
        }
    };

    this.fontWeight = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontWeight = value;
        } else {
            return parseInt(self.domElement.style.fontWeight);
        }
    };

    this.fontVariant = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontVariant = value;
        } else {
            return parseInt(self.domElement.style.fontVariant);
        }
    };

    this.fontStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.fontStyle = value;
        } else {
            return parseInt(self.domElement.style.fontStyle);
        }
    };

    this.height = function (value) {
        if (value !== undefined) {
            self.domElement.style.height = value;
        } else {
            return (self.domElement.style.height);
        }
    };

    this.hidden = function (value) {
        if (value !== undefined) {
            self.domElement.hidden = value;
        } else {
            return (self.domElement.hidden);
        }
    };

    this.innerText = function (text) {
        if (text !== undefined) {
            self.domElement.innerText = text;
        } else {
            return (self.domElement.innerText);
        }
    };

    this.innerHTML = function (text) {
        if (text !== undefined) {
            self.domElement.innerHTML = text;
        } else {
            return (self.domElement.innerHTML);
        }
    };

    this.justifyContent = function (value) {
        if (value !== undefined) {
            self.domElement.style.justifyContent = value;
        } else {
            return parseInt(self.domElement.style.justifyContent);
        }
    };

    this.left = function (value) {
        if (value !== undefined) {
            self.domElement.style.left = value;
        } else {
            return ((self.domElement.style.left) || self.domElement.getBoundingClientRect().left);
        }
    };

    this.letterSpacing = function (value) {
        if (value !== undefined) {
            self.domElement.style.letterSpacing = value;
        } else {
            return parseInt(self.domElement.style.letterSpacing);
        }
    };

    this.lineHeight = function (value) {
        if (value !== undefined) {
            self.domElement.style.lineHeight = value;
        } else {
            return (self.domElement.style.lineHeight);
        }
    };

    this.listStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.listStyle = value;
        } else {
            return parseInt(self.domElement.style.listStyle);
        }
    };

    this.listStyleImage = function (value) {
        if (value !== undefined) {
            self.domElement.style.listStyleImage = value;
        } else {
            return parseInt(self.domElement.style.listStyleImage);
        }
    };

    this.listStyleType = function (value) {
        if (value !== undefined) {
            self.domElement.style.listStyleType = value;
        } else {
            return parseInt(self.domElement.style.listStyleType);
        }
    };

    this.marginTRBL = function (top, right, bottom, left) {
        if (top !== undefined) {
            self.domElement.style.marginTop = top + "px";
        }
        if (right !== undefined) {
            self.domElement.style.marginRight = right + "px";
        }
        if (bottom !== undefined) {
            self.domElement.style.marginBottom = bottom + "px";
        }
        if (left !== undefined) {
            self.domElement.style.marginLeft = left + "px";
        }
    };

    this.margin = function (value) {
        if (value !== undefined) {
            self.domElement.style.margin = value;
        } else {
            return (self.domElement.style.margin);
        }
    };

    this.marginBottom = function (value) {
        if (value !== undefined) {
            self.domElement.style.marginBottom = value;
        } else {
            return parseInt(self.domElement.style.marginBottom);
        }
    };

    this.marginLeft = function (value) {
        if (value !== undefined) {
            self.domElement.style.marginLeft = value;
        } else {
            return parseInt(self.domElement.style.marginLeft);
        }
    };

    this.marginRight = function (value) {
        if (value !== undefined) {
            self.domElement.style.marginRight = value;
        } else {
            return parseInt(self.domElement.style.marginRight);
        }
    };

    this.marginTop = function (value) {
        if (value !== undefined) {
            self.domElement.style.marginTop = value;
        } else {
            return parseInt(self.domElement.style.marginTop);
        }
    };

    this.maxHeight = function (value) {
        if (value !== undefined) {
            self.domElement.style.maxHeight = value;
        } else {
            return parseInt(self.domElement.style.maxHeight);
        }
    };

    this.maxWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.maxWidth = value;
        } else {
            return parseInt(self.domElement.style.maxWidth);
        }
    };

    this.minHeight = function (value) {
        if (value !== undefined) {
            self.domElement.style.minHeight = value;
        } else {
            return parseInt(self.domElement.style.minHeight);
        }
    };

    this.minWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.minWidth = value;
        } else {
            return parseInt(self.domElement.style.minWidth);
        }
    };

    this.opacity = function (value) {
        if (value !== undefined) {
            self.domElement.style.opacity = value;
        } else {
            return parseInt(self.domElement.style.opacity);
        }
    };

    this.order = function (value) {
        if (value !== undefined) {
            self.domElement.style.order = value;
        } else {
            return parseInt(self.domElement.style.order);
        }
    };

    this.outline = function (value) {
        if (value !== undefined) {
            self.domElement.style.outline = value;
        } else {
            return parseInt(self.domElement.style.outline);
        }
    };

    this.outlineColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.outlineColor = value;
        } else {
            return parseInt(self.domElement.style.outlineColor);
        }
    };

    this.outlineOffset = function (value) {
        if (value !== undefined) {
            self.domElement.style.outlineOffset = value;
        } else {
            return parseInt(self.domElement.style.outlineOffset);
        }
    };

    this.outlineStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.outlineStyle = value;
        } else {
            return parseInt(self.domElement.style.outlineStyle);
        }
    };

    this.outlineWidth = function (value) {
        if (value !== undefined) {
            self.domElement.style.outlineWidth = value;
        } else {
            return parseInt(self.domElement.style.outlineWidth);
        }
    };

    this.overflow = function (value) {
        if (value !== undefined) {
            self.domElement.style.overflow = value;
        } else {
            return parseInt(self.domElement.style.overflow);
        }
    };

    this.overflowX = function (value) {
        if (value !== undefined) {
            self.domElement.style.overflowX = value;
        } else {
            return parseInt(self.domElement.style.overflowX);
        }
    };

    this.overflowY = function (value) {
        if (value !== undefined) {
            self.domElement.style.overflowY = value;
        } else {
            return parseInt(self.domElement.style.overflowY);
        }
    };

    this.padding = function (value) {
        if (value !== undefined) {
            self.domElement.style.padding = value;
        } else {
            (self.domElement.style.padding);
        }
    };

    this.paddingBottom = function (value) {
        if (value !== undefined) {
            self.domElement.style.paddingBottom = value;
        } else {
            return parseInt(self.domElement.style.paddingBottom);
        }
    };

    this.paddingLeft = function (value) {
        if (value !== undefined) {
            self.domElement.style.paddingLeft = value;
        } else {
            return parseInt(self.domElement.style.paddingLeft);
        }
    };

    this.paddingRight = function (value) {
        if (value !== undefined) {
            self.domElement.style.paddingRight = value;
        } else {
            return parseInt(self.domElement.style.paddingRight);
        }
    };

    this.paddingTop = function (value) {
        if (value !== undefined) {
            self.domElement.style.paddingTop = value;
        } else {
            return parseInt(self.domElement.style.paddingTop);
        }
    };

    this.pageBreakAfter = function (value) {
        if (value !== undefined) {
            self.domElement.style.pageBreakAfter = value;
        } else {
            return parseInt(self.domElement.style.pageBreakAfter);
        }
    };

    this.pageBreakBefore = function (value) {
        if (value !== undefined) {
            self.domElement.style.pageBreakBefore = value;
        } else {
            return parseInt(self.domElement.style.pageBreakBefore);
        }
    };

    this.pageBreakInside = function (value) {
        if (value !== undefined) {
            self.domElement.style.pageBreakInside = value;
        } else {
            return parseInt(self.domElement.style.pageBreakInside);
        }
    };

    this.perspective = function (value) {
        if (value !== undefined) {
            self.domElement.style.perspective = value;
        } else {
            return parseInt(self.domElement.style.perspective);
        }
    };

    this.perspectiveOrigin = function (value) {
        if (value !== undefined) {
            self.domElement.style.perspectiveOrigin = value;
        } else {
            return parseInt(self.domElement.style.perspectiveOrigin);
        }
    };

    this.placeholder = function (value) {
        if (value !== undefined) {
            self.domElement.placeholder = value;
        } else {
            return (self.domElement.placeholder);
        }
    };

    this.pointerEvents = function (value) {
        if (value !== undefined) {
            self.domElement.style.pointerEvents = value;
        } else {
            return (self.domElement.style.pointerEvents);
        }
    };

    this.position = function (value) {
        if (value !== undefined) {
            self.domElement.style.position = value;
        } else {
            return (self.domElement.style.position);
        }
    };

    this.quotes = function (value) {
        if (value !== undefined) {
            self.domElement.style.quotes = value;
        } else {
            return parseInt(self.domElement.style.quotes);
        }
    };

    this.resize = function (value) {
        if (value !== undefined) {
            self.domElement.style.resize = value;
        } else {
            return (self.domElement.style.resize);
        }
    };

    this.right = function (value) {
        if (value !== undefined) {
            self.domElement.style.right = value;
        } else {
            return (parseInt(self.domElement.style.right) || self.domElement.getBoundingClientRect().right);
        }
    };

    this.tableLayout = function (value) {
        if (value !== undefined) {
            self.domElement.style.tableLayout = value;
        } else {
            return parseInt(self.domElement.style.tableLayout);
        }
    };

    this.textAlign = function (value) {
        if (value !== undefined) {
            self.domElement.style.textAlign = value;
        } else {
            return (self.domElement.style.textAlign);
        }
    };

    this.textAlignLast = function (value) {
        if (value !== undefined) {
            self.domElement.style.textAlignLast = value;
        } else {
            return (self.domElement.style.textAlignLast);
        }
    };

    this.textDecoration = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecoration = value;
        } else {
            return parseInt(self.domElement.style.textDecoration);
        }
    };

    this.textDecorationLineThrough = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationLineThrough = value;
        } else {
            return parseInt(self.domElement.style.textDecorationLineThrough);
        }
    };

    this.textDecorationNone = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationNone = value;
        } else {
            return parseInt(self.domElement.style.textDecorationNone);
        }
    };

    this.textDecorationOverLine = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationOverLine = value;
        } else {
            return parseInt(self.domElement.style.textDecorationOverLine);
        }
    };

    this.textDecorationUnderline = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationUnderline = value;
        } else {
            return parseInt(self.domElement.style.textDecorationUnderline);
        }
    };

    this.textDecorationColor = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationColor = value;
        } else {
            return parseInt(self.domElement.style.textDecorationColor);
        }
    };

    this.textDecorationLine = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationLine = value;
        } else {
            return parseInt(self.domElement.style.textDecorationLine);
        }
    };

    this.textDecorationStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.textDecorationStyle = value;
        } else {
            return parseInt(self.domElement.style.textDecorationStyle);
        }
    };

    this.textIndent = function (value) {
        if (value !== undefined) {
            self.domElement.style.textIndent = value;
        } else {
            return parseInt(self.domElement.style.textIndent);
        }
    };

    this.textOverflow = function (value) {
        if (value !== undefined) {
            self.domElement.style.textOverflow = value;
        } else {
            return parseInt(self.domElement.style.textOverflow);
        }
    };

    this.textShadow = function (value) {
        if (value !== undefined) {
            self.domElement.style.textShadow = value;
        } else {
            return parseInt(self.domElement.style.textShadow);
        }
    };

    this.textTransform = function (value) {
        if (value !== undefined) {
            self.domElement.style.textTransform = value;
        } else {
            return parseInt(self.domElement.style.textTransform);
        }
    };

    this.top = function (value) {
        if (value !== undefined) {
            self.domElement.style.top = value;
        } else {
            return ((self.domElement.style.top) || self.domElement.getBoundingClientRect().top);
        }
    };

    this.transform = function (value) {
        if (value !== undefined) {
            self.domElement.style.transform = value;
        } else {
            return parseInt(self.domElement.style.transform);
        }
    };

    this.transformOrigin = function (value) {
        if (value !== undefined) {
            self.domElement.style.transformOrigin = value;
        } else {
            return parseInt(self.domElement.style.transformOrigin);
        }
    };

    this.transformStyle = function (value) {
        if (value !== undefined) {
            self.domElement.style.transformStyle = value;
        } else {
            return parseInt(self.domElement.style.transformStyle);
        }
    };

    this.transition = function (value) {
        if (value !== undefined) {
            self.domElement.style.transition = value;
        } else {
            return (self.domElement.style.transition);
        }
    };

    this.webkitTransition = function (value) {
        if (value !== undefined) {
            self.domElement.style.webkitTransition = value;
        } else {
            return (self.domElement.style.webkitTransition);
        }
    };

    this.transitionDelay = function (value) {
        if (value !== undefined) {
            self.domElement.style.transitionDelay = value;
        } else {
            return (self.domElement.style.transitionDelay);
        }
    };

    this.transitionTimingFunction = function (value) {
        if (value !== undefined) {
            self.domElement.style.transitionTimingFunction = value;
        } else {
            return (self.domElement.style.transitionTimingFunction);
        }
    };

    this.transitionProperty = function (value) {
        if (value !== undefined) {
            self.domElement.style.transitionProperty = value;
        } else {
            return (self.domElement.style.transitionProperty);
        }
    };

    this.transitionDuration = function (value) {
        if (value !== undefined) {
            self.domElement.style.transitionDuration = value;
        } else {
            return (self.domElement.style.transitionDuration);
        }
    };

    this.type = function (value) {
        if (value !== undefined) {
            self.domElement.type = value;
        } else {
            return parseInt(self.domElement.type);
        }
    };

    this.value = function (value) {
        if (value !== undefined) {
            self.domElement.value = value;
        } else {
            return (self.domElement.value);
        }
    };

    this.verticalAlign = function (value) {
        if (value !== undefined) {
            self.domElement.style.verticalAlign = value;
        } else {
            return parseInt(self.domElement.style.verticalAlign);
        }
    };

    this.visibility = function (value) {
        if (value !== undefined) {
            self.domElement.style.visibility = value;
        } else {
            return parseInt(self.domElement.style.visibility);
        }
    };

    this.webkitUserSelect = function (value) {
        if (value !== undefined) {
            self.domElement.style.webkitUserSelect = value;
        } else {
            return parseInt(self.domElement.style.webkitUserSelect);
        }
    };

    this.UserSelect = function (value) {
        if (value !== undefined) {
            self.domElement.style.UserSelect = value;
        } else {
            return parseInt(self.domElement.style.UserSelect);
        }
    };

    this.mozUserSelect = function (value) {
        if (value !== undefined) {
            self.domElement.style.mozUserSelect = value;
        } else {
            return parseInt(self.domElement.style.mozUserSelect);
        }
    };

    this.msUserSelect = function (value) {
        if (value !== undefined) {
            self.domElement.style.msUserSelect = value;
        } else {
            return parseInt(self.domElement.style.msUserSelect);
        }
    };

    this.whiteSpace = function (value) {
        if (value !== undefined) {
            self.domElement.style.whiteSpace = value;
        } else {
            return parseInt(self.domElement.style.whiteSpace);
        }
    };

    this.width = function (value) {
        if (value !== undefined) {
            self.domElement.style.width = value;
        } else {
            return (self.domElement.style.width);
        }
    };

    this.wordBreak = function (value) {
        if (value !== undefined) {
            self.domElement.style.wordBreak = value;
        } else {
            return parseInt(self.domElement.style.wordBreak);
        }
    };

    this.wordSpacing = function (value) {
        if (value !== undefined) {
            self.domElement.style.wordSpacing = value;
        } else {
            return parseInt(self.domElement.style.wordSpacing);
        }
    };

    this.wordWrap = function (value) {
        if (value !== undefined) {
            self.domElement.style.wordWrap = value;
        } else {
            return parseInt(self.domElement.style.wordWrap);
        }
    };

    this.writingMode = function (value) {
        if (value !== undefined) {
            self.domElement.style.writingMode = value;
        } else {
            return parseInt(self.domElement.style.writingMode);
        }
    };

    this.zIndex = function (value) {
        if (value !== undefined) {
            self.domElement.style.zIndex = value;
        } else {
            return parseInt(self.domElement.style.zIndex);
        }
    };

};

