var formDiv = function (target) {
    Basestyle.apply(this, arguments);
    formDiv.prototype = Object.create(Basestyle.prototype);
    formDiv.prototype.constructor = formDiv;
    
    var self = this;

    this.enabled = function (value) {
        if (value) {
            self.domElement.disabled = false;
        } else {
            self.domElement.disabled = true;
        }
    };

    var draw = function () {
        var form = document.createElement('form');
        self.box = form;
        self.domElement = form;
        target.domElement.appendChild(form);
    };
    
    draw();
    
};

formDiv.prototype.style = {};

