var Redactor = function (target, mode) {
    Basestyle.apply(this, arguments);
    Redactor.prototype = Object.create(Basestyle.prototype);
    Redactor.prototype.constructor = Redactor;

    this.domElement;
    this.elements = [];
    this.onClick;
    this.hide;
    var self = this;
    this.statMode = mode;

    this.draw = function () {
        switch (this.mode()) {
            case 1:
                this.changeStyleWs();
                break;
            case 2:
                this.changeStyleForm();
                break;
            case 3:
                this.helpStyleForm();
                break;
            case 4:
                this.changeBox();
                break;
        }
    };

    this.mode = function (value) {
        if (value !== undefined) {
            this.statMode = value;
            this.draw();
        } else {
            return parseInt(this.statMode);
        }
    };

    this.changeStyleWs = function () {
        
        var box = new Box(target);
        box.mode = 1;
        self.box = box;
        self.domElement = box.domElement;

        var head = new Box(box);
        self.head = head;

        var closeBtn = new Button(head);
        self.closeBtn = closeBtn;

        var innerBox = new Box(box);
        self.innerBox = innerBox;

        var boxInner = new formDiv(innerBox);
        self.boxInner = boxInner;

        var textColor = new Form(boxInner, undefined, 1);
        self.textColor = textColor;
        
        var textInner = new Form(boxInner, undefined, 1);
        self.textInner = textInner;

        var borderBackground = new Form(boxInner, undefined, 1);
        self.borderBackground = borderBackground;

        var boxButton = new Box(boxInner);
        self.boxButton = boxButton;

        var reset = new Button(boxButton);
        self.reset = reset;

        var submit = new Button(boxButton);
        self.submit = submit;

        self.closeBtn.domElement.onclick = function () {
            self.hidden(true);
        };

        self.submit.domElement.onclick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.onClick && self.onClick();

        };

        self.submit.domElement.onmouseup = function () {
            self.hidden(true);
        };

        self.reset.domElement.onclick = function () {
            self.boxInner.domElement.reset();
        };

    };

    this.changeStyleForm = function () {
        
        var box = new Box(target);
        box.mode = 2;
        self.box = box;
        self.domElement = box.domElement;

        var head = new Box(box);
        self.head = head;

        var closeBtn = new Button(head);
        self.closeBtn = closeBtn;

        var innerBox = new Box(box);
        self.innerBox = innerBox;

        var boxInner = new formDiv(innerBox);
        self.boxInner = boxInner;

        var backgroundForm = new Form(boxInner, undefined, 1);
        self.backgroundForm = backgroundForm;

        var innerTextLabel = new Form(boxInner, undefined, 1);
        self.innerTextLabel = innerTextLabel;

        var borderColor = new Form(boxInner, undefined, 1);
        self.borderColor = borderColor;

        var colorText = new Form(boxInner, undefined, 1);
        self.colorText = colorText;

        var boxButton = new Box(boxInner);
        self.boxButton = boxButton;

        var resetForm = new Button(boxButton);
        self.resetForm = resetForm;

        var submit = new Button(boxButton);
        self.submit = submit;

        self.closeBtn.domElement.onclick = function () {
            self.hidden(true);
        };

        self.submit.domElement.onclick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.onClick && self.onClick();
            self.hidden(true);
        };

        self.resetForm.domElement.onclick = function () {
            self.boxInner.domElement.reset();
        };

    };

    this.helpStyleForm = function () {
        
        var box = new Box(target);
        box.mode = 3;
        self.box = box;
        self.domElement = box.domElement;

        var head = new Box(box);
        self.head = head;

        var closeBtn = new Button(head);
        self.closeBtn = closeBtn;

        var innerBox = new Box(box);
        self.innerBox = innerBox;

        var boxInner = new formDiv(innerBox);
        self.boxInner = boxInner;
        
        var textColor = new formDiv(boxInner);
        self.textColor = textColor;

        self.closeBtn.domElement.onclick = function () {
            self.hidden(true);
        };

    };

    this.changeBox = function () {
        var box = new Box(target);
        box.mode = 4;
        self.box = box;
        self.domElement = box.domElement;
        box.transitionProperty('background');
        box.transitionDuration('5s');
        self.box.domElement.onclick = function () {
            self.hidden(true);
            hide();
        };
    };

    this.draw();

};